﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows;

namespace AmaInput
{
    public partial class FormNoScanCheck : Form
    {
        DataTable m_DataTable = null;
        DataView m_DataView = null;
        
        class comboData
        {
            public string text;
            public int cnt = 0;
            public comboData(string text)
            {
                this.text = text;
            }
            public List<comboData> subData = new List<comboData>();

            override public string ToString()
            {
                if (cnt > 0) return text + " (" + cnt + "件)";
                else return text;
            }
        }
        List<comboData> m_monthList = new List<comboData>();


        public FormNoScanCheck()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 起動時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormTablePreview_Load(object sender, EventArgs e)
        {

            try
            {
                C_GlobalData.DataGridView_AddCripbordCopyMenu(dataGridView1);

                string query = " select 審査年 || '年' || 審査月 || '月審査' AS 審査月, 束番号, 子番号, 機関番号, 機関名, 被保険者番号, 受診者氏名, 施療年月, 決定金額" +
                               " from tdata where 処理番号 NOT IN " +
                               " (SELECT 処理番号 FROM tdata, receipts WHERE 処理番号 = rid) " +
                               " ORDER BY 審査年 DESC, 審査月 DESC, 束番号, 子番号 ";
                using (var da = SQLClass.GetDbDataAdapter(query))
                {
                    m_DataTable = new DataTable();
                    da.Fill(m_DataTable);
                }

                m_monthList.Add(new comboData("審査月"));
                m_monthList.Last().subData.Add(new comboData("束番号"));


                for (int i = 0; i < m_DataTable.Rows.Count; i++)
                {
                    if (i == 0 ||
                         m_DataTable.Rows[i]["審査月"].ToString() != m_DataTable.Rows[i - 1]["審査月"].ToString())
                    {
                        m_monthList.Add(new comboData(m_DataTable.Rows[i]["審査月"].ToString()));
                        m_monthList.Last().subData.Add(new comboData("束番号"));
                        m_monthList.Last().subData.Add(new comboData(m_DataTable.Rows[i]["束番号"].ToString()));
                    }
                    else if (m_DataTable.Rows[i]["束番号"].ToString() != m_DataTable.Rows[i - 1]["束番号"].ToString())
                    {
                        m_monthList.Last().subData.Add(new comboData(m_DataTable.Rows[i]["束番号"].ToString()));
                    }

                    m_monthList.Last().cnt++;
                    m_monthList.Last().subData.Last().cnt++;
                }


                comboBoxBillMonth.DataSource = m_monthList;
                label1.Text = "スキャン漏れデータ : " + m_DataTable.Rows.Count + " 件";

            }
            catch (Exception ex)
            {
                Logs.writeErr("エラーが発生しました", true);
                Logs.writeErr(ex, false);
                Close();
            }

        }

        private void comboBoxBillMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            try 
	        {
                comboBoxTaba.DataSource = (comboBoxBillMonth.SelectedValue as comboData).subData;
	        }
	        catch (Exception ex)
	        {
                Logs.writeErr(ex, false);
	        }
        }

        private void comboBoxTaba_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                dataGridView1.DataSource = null;
                if (m_DataView != null) m_DataView.Dispose();

                if (comboBoxBillMonth.SelectedIndex <= 0)
                {
                    // 審査月指定なし。
                    dataGridView1.DataSource = m_DataTable;
                }
                else if (comboBoxTaba.SelectedIndex <= 0)
                {
                    m_DataView = new DataView(m_DataTable, 
                                              "審査月 = '" + (comboBoxBillMonth.SelectedItem as comboData).text + "'",
                                              "審査月 DESC, 束番号, 子番号",
                                              DataViewRowState.CurrentRows);
                    dataGridView1.DataSource = m_DataView;
                }
                else
                {
                    m_DataView = new DataView(m_DataTable,
                                              "審査月 = '" + (comboBoxBillMonth.SelectedItem as comboData).text + "' AND " +
                                              "束番号 = '" + (comboBoxTaba.SelectedItem as comboData).text + "'",
                                              "審査月 DESC, 束番号, 子番号",
                                              DataViewRowState.CurrentRows);
                    dataGridView1.DataSource = m_DataView;
                }

                dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);
            }
            catch (Exception ex)
            {
                Logs.writeErr(ex, false);
            }
        }
        
    }
}
