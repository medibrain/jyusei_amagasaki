﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

public partial class ScanRename : Form
{
    public ScanRename()
    {
        InitializeComponent();
    }

    private void Form1_Load(object sender, EventArgs e)
    {
    }
    
    /// <summary> 取込実行ボタン </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void button2_Click(object sender, EventArgs e)
    {
        try
        {
            // 年、月、簿冊番号のチェック
            var textbox = new[] { textBox1, textBox2, textBox3 };  // 年、月、簿冊番号のテキストボックス
            int[] n = new int[textbox.Length];                     // 年、月、簿冊番号の入力値
            int[] min = new[] { 20, 1, 0 };                        // 年、月、簿冊番号の制限（MIN）
            int[] max = new[] { 99, 12, 999 };                     // 年、月、簿冊番号の制限（MAX）

            for (int i = 0; i < textbox.Length; i++)
            {
                if (textbox[i].Text.Length != textbox[i].MaxLength || int.TryParse(textbox[i].Text, out n[i]) == false || n[i] < min[i] || n[i] > max[i])
                {
                    textbox[i].Select();
                    textbox[i].SelectAll();
                    return;
                }
            }



            // スキャン元フォルダから、ファイル一覧を取得する。
            var imgFiles = ImageFileList(C_GlobalData.GetScanDir());
            string group = textBox1.Text + "-" + textBox2.Text + "-" + textBox3.Text;

            string destDir = C_GlobalData.GetPictureDir() + @"\" + group;
            Directory.CreateDirectory(destDir);

            int cnt = ImageFileList(destDir).Length;                 // スキャン済のカウント
            if (MessageBox.Show(this,
                                "取込確認\r\n" +
                                 group + "\r\n" +
                                "取込元：" + imgFiles.Length + "枚\r\n" +
                                "取込先：" + cnt + "枚 → " + (cnt + imgFiles.Length).ToString() + "枚\r\n" +
                                "取り込みますか？", "取込確認", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {


                foreach (var from in imgFiles)
                {
                    string destPath = destDir + "\\" + group + "-" + (++cnt).ToString("000") + Path.GetExtension(from);
                    File.Move(from, destPath);

			    }
            }

            // DBへの登録
            var fileList = new HashSet<string>();
            using (var c = SQLClass.CreateCommand("SELECT fname FROM receipts WHERE gname = :gname"))
            {
                c.Parameters.Add("gname", NpgsqlTypes.NpgsqlDbType.Text);
                c.Parameters["gname"].Value = group;
                using (var r = c.ExecuteReader())
                {
                    while (r.Read())
                    {
                        fileList.Add(r[0].ToString());
                    }
                }
            }


            using (var c = SQLClass.CreateCommand("INSERT INTO receipts (gname, fname) VALUES (:gname, :fname)"))
            {
                c.Parameters.Add("gname", NpgsqlTypes.NpgsqlDbType.Text);
                c.Parameters.Add("fname", NpgsqlTypes.NpgsqlDbType.Text);
                c.Parameters["gname"].Value = group;


                foreach (var path in ImageFileList(destDir))
                {
                    var fname = Path.GetFileName(path);
                    if (fileList.Contains(fname) == false)
                    {
                        c.Parameters["fname"].Value = fname;
                        c.ExecuteNonQuery();
                    }
                }
            }

            textBox1.Text = textBox2.Text = textBox3.Text = "";
            System.Diagnostics.Process.Start(destDir);

        }
        catch (Exception ex)
        {
            File.AppendAllText( C_GlobalData.GetPictureDir() + @"\スキャンエラー.txt",
                               DateTime.Now + "\r\n" +
                               "スキャン元：" + C_GlobalData.GetScanDir() + "\r\n" +
                               "スキャン先：" + textBox1.Text + "-" + textBox2.Text + "-" + textBox3.Text + "\r\n" + 
                               ex.Message + "\r\n" + 
                               ex.StackTrace + "\r\n\r\n", Encoding.Default);

            MessageBox.Show(this, "エラー\r\n\r\n" + ex.Message, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }

    /// <summary> 画像ファイル一覧の取得</summary>
    /// <param name="dir">ディレクトリ名</param>
    /// <returns>画像ファイル一覧</returns>
    static string[] ImageFileList(string dir)
    {
        var imgFiles = new List<string>();
        foreach (var ext in new[] { "*.tif", "*.tiff", "*.gif", "*.png", "*.jpg", "*.jpeg", "*.bmp" }) imgFiles.AddRange(Directory.GetFiles(dir, ext));
        imgFiles.Sort();
        return imgFiles.ToArray();
    }


    /// <summary> スキャン元確認 </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void button1_Click(object sender, EventArgs e)
    {
        System.Diagnostics.Process.Start(C_GlobalData.GetScanDir());
    }

}
