﻿namespace AmaInput
{
    partial class FormInput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonCounterclockwise = new System.Windows.Forms.Button();
            this.buttonClockwise = new System.Windows.Forms.Button();
            this.labelFname = new System.Windows.Forms.Label();
            this.labelBosatsu = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxKo = new System.Windows.Forms.TextBox();
            this.textBoxHiho = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonPrev = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.userControlImage1 = new UserControlImage();
            this.userControlImage2 = new UserControlImage();
            this.userControlImage3 = new UserControlImage();
            this.userControlImage4 = new UserControlImage();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.buttonCounterclockwise);
            this.panel1.Controls.Add(this.buttonClockwise);
            this.panel1.Controls.Add(this.labelFname);
            this.panel1.Controls.Add(this.labelBosatsu);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.labelName);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.textBoxKo);
            this.panel1.Controls.Add(this.textBoxHiho);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.buttonPrev);
            this.panel1.Controls.Add(this.buttonOK);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(631, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(176, 467);
            this.panel1.TabIndex = 0;
            // 
            // buttonCounterclockwise
            // 
            this.buttonCounterclockwise.BackColor = System.Drawing.Color.White;
            this.buttonCounterclockwise.Image = global::AmaInput.Properties.Resources.ImageCounterclockwise;
            this.buttonCounterclockwise.Location = new System.Drawing.Point(56, 8);
            this.buttonCounterclockwise.Name = "buttonCounterclockwise";
            this.buttonCounterclockwise.Size = new System.Drawing.Size(50, 48);
            this.buttonCounterclockwise.TabIndex = 1;
            this.buttonCounterclockwise.UseVisualStyleBackColor = false;
            this.buttonCounterclockwise.Click += new System.EventHandler(this.buttonRotate_Click);
            // 
            // buttonClockwise
            // 
            this.buttonClockwise.BackColor = System.Drawing.Color.White;
            this.buttonClockwise.Image = global::AmaInput.Properties.Resources.ImageClockwise;
            this.buttonClockwise.Location = new System.Drawing.Point(106, 8);
            this.buttonClockwise.Name = "buttonClockwise";
            this.buttonClockwise.Size = new System.Drawing.Size(50, 48);
            this.buttonClockwise.TabIndex = 2;
            this.buttonClockwise.UseVisualStyleBackColor = false;
            this.buttonClockwise.Click += new System.EventHandler(this.buttonRotate_Click);
            // 
            // labelFname
            // 
            this.labelFname.AutoSize = true;
            this.labelFname.Font = new System.Drawing.Font("MS UI Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelFname.Location = new System.Drawing.Point(10, 359);
            this.labelFname.Name = "labelFname";
            this.labelFname.Size = new System.Drawing.Size(108, 21);
            this.labelFname.TabIndex = 13;
            this.labelFname.Text = "labelFname";
            // 
            // labelBosatsu
            // 
            this.labelBosatsu.AutoSize = true;
            this.labelBosatsu.Font = new System.Drawing.Font("MS UI Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelBosatsu.Location = new System.Drawing.Point(10, 325);
            this.labelBosatsu.Name = "labelBosatsu";
            this.labelBosatsu.Size = new System.Drawing.Size(121, 21);
            this.labelBosatsu.TabIndex = 12;
            this.labelBosatsu.Text = "labelBosatsu";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 0;
            this.label6.Text = "画像回転";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 313);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 11;
            this.label5.Text = "簿冊番号";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(9, 159);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(110, 12);
            this.labelName.TabIndex = 8;
            this.labelName.Text = "labelName 漢字氏名";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 219);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 12);
            this.label1.TabIndex = 8;
            this.label1.Text = "(不鮮明な場合は空白)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 36);
            this.label4.TabIndex = 5;
            this.label4.Text = "被保険者番号７桁\r\n続紙[ ------- ]\r\nエラー[ ******* ]";
            // 
            // textBoxKo
            // 
            this.textBoxKo.Font = new System.Drawing.Font("ＭＳ ゴシック", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxKo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxKo.Location = new System.Drawing.Point(59, 184);
            this.textBoxKo.MaxLength = 7;
            this.textBoxKo.Name = "textBoxKo";
            this.textBoxKo.Size = new System.Drawing.Size(87, 28);
            this.textBoxKo.TabIndex = 7;
            this.textBoxKo.Enter += new System.EventHandler(this.textBox_Enter);
            this.textBoxKo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
            this.textBoxKo.Leave += new System.EventHandler(this.textBox_Leave);
            this.textBoxKo.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxHiho_Validating);
            // 
            // textBoxHiho
            // 
            this.textBoxHiho.Font = new System.Drawing.Font("ＭＳ ゴシック", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxHiho.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxHiho.Location = new System.Drawing.Point(56, 83);
            this.textBoxHiho.MaxLength = 7;
            this.textBoxHiho.Name = "textBoxHiho";
            this.textBoxHiho.Size = new System.Drawing.Size(87, 28);
            this.textBoxHiho.TabIndex = 4;
            this.textBoxHiho.TextChanged += new System.EventHandler(this.textBoxHiho_TextChanged);
            this.textBoxHiho.Enter += new System.EventHandler(this.textBox_Enter);
            this.textBoxHiho.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
            this.textBoxHiho.Leave += new System.EventHandler(this.textBox_Leave);
            this.textBoxHiho.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxHiho_Validating);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 195);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "子番号";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "被保番";
            // 
            // buttonPrev
            // 
            this.buttonPrev.Location = new System.Drawing.Point(29, 249);
            this.buttonPrev.Name = "buttonPrev";
            this.buttonPrev.Size = new System.Drawing.Size(52, 43);
            this.buttonPrev.TabIndex = 9;
            this.buttonPrev.TabStop = false;
            this.buttonPrev.Text = "戻る";
            this.buttonPrev.UseVisualStyleBackColor = true;
            this.buttonPrev.Click += new System.EventHandler(this.buttonPrev_Click);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(91, 249);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(52, 43);
            this.buttonOK.TabIndex = 10;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.userControlImage1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(631, 467);
            this.splitContainer1.SplitterDistance = 416;
            this.splitContainer1.TabIndex = 1;
            // 
            // splitContainer2
            // 
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.splitContainer3);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.userControlImage4);
            this.splitContainer2.Size = new System.Drawing.Size(209, 465);
            this.splitContainer2.SplitterDistance = 311;
            this.splitContainer2.TabIndex = 0;
            // 
            // splitContainer3
            // 
            this.splitContainer3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.userControlImage2);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.userControlImage3);
            this.splitContainer3.Size = new System.Drawing.Size(209, 311);
            this.splitContainer3.SplitterDistance = 153;
            this.splitContainer3.TabIndex = 0;
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Image = null;
            this.userControlImage1.ImageLocation = null;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(414, 465);
            this.userControlImage1.TabIndex = 0;
            // 
            // userControlImage2
            // 
            this.userControlImage2.AutoScroll = true;
            this.userControlImage2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage2.Image = null;
            this.userControlImage2.ImageLocation = null;
            this.userControlImage2.Location = new System.Drawing.Point(0, 0);
            this.userControlImage2.Name = "userControlImage2";
            this.userControlImage2.Size = new System.Drawing.Size(205, 149);
            this.userControlImage2.TabIndex = 1;
            // 
            // userControlImage3
            // 
            this.userControlImage3.AutoScroll = true;
            this.userControlImage3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage3.Image = null;
            this.userControlImage3.ImageLocation = null;
            this.userControlImage3.Location = new System.Drawing.Point(0, 0);
            this.userControlImage3.Name = "userControlImage3";
            this.userControlImage3.Size = new System.Drawing.Size(205, 150);
            this.userControlImage3.TabIndex = 1;
            // 
            // userControlImage4
            // 
            this.userControlImage4.AutoScroll = true;
            this.userControlImage4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage4.Image = null;
            this.userControlImage4.ImageLocation = null;
            this.userControlImage4.Location = new System.Drawing.Point(0, 0);
            this.userControlImage4.Name = "userControlImage4";
            this.userControlImage4.Size = new System.Drawing.Size(205, 146);
            this.userControlImage4.TabIndex = 1;
            // 
            // FormInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(807, 467);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.Name = "FormInput";
            this.Text = "入力";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormInput_FormClosing);
            this.Load += new System.EventHandler(this.FormInput_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonPrev;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxHiho;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelBosatsu;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Label labelFname;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonClockwise;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.Button buttonCounterclockwise;
        private System.Windows.Forms.TextBox textBoxKo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelName;
        private UserControlImage userControlImage1;
        private UserControlImage userControlImage2;
        private UserControlImage userControlImage3;
        private UserControlImage userControlImage4;
    }
}