﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace AmaInput
{
    static class Program
    {
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main()
        {

            Logs.Begin();
            try
            {
                SQLClass.Open();
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1());
                SQLClass.Close();

            }
            catch (Exception ex)
            {
                Logs.writeErr(ex, false);
                Logs.writeErr("エラーが発生しました。", true);
            }

            Logs.End();
        }
    }
}
