﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

/// <summary>
/// ログ出力クラス
/// </summary>
public class Logs
{
    public class LogSetting
    {
        public bool log = true;
        public bool err = false;
        public bool timer = false;
        public bool qrlog = true;
        public bool qrerr = true;
    }


    /// <summary> 通常ログの出力キュー（nullの場合は出力しない。）</summary>
    static List<string> logQueue;

    /// <summary> QR読み込みログの出力キュー（nullの場合は出力しない。）</summary>
    static List<string> qrlogQueue;
    static List<string> qrerrQueue;

    /// <summary> エラーログの出力キュー（nullの場合は出力しない。）</summary>
    static List<string> errQueue;

    /// <summary> エラーログの出力キュー（nullの場合は出力しない。）</summary>
    static List<string> timerQueue;




    /// <summary> 開始時刻 </summary>
    static DateTime beginTime;

    /// <summary> コンストラクタ </summary>
    public static void Begin()
    {
        beginTime = DateTime.Now;

        string logDir = C_GlobalData.GetPictureDir() + @"\debug";

        try
        {
            // ログ保存用のディレクトリを作成する。
            if (Directory.Exists(logDir) == false) Directory.CreateDirectory(logDir);
        }
        catch (Exception ex)
        {
            System.Diagnostics.Trace.WriteLine(ex.StackTrace);
        }


        var logSettings = C_GlobalData.readXML<LogSetting>(logDir + @"\LogSetting.xml");
        logQueue = (logSettings.log) ? new List<string>() : null;
        qrerrQueue = (logSettings.qrerr) ? new List<string>() : null;
        qrlogQueue = (logSettings.qrlog) ? new List<string>() : null;
        errQueue = (logSettings.err) ? new List<string>() : null;
        timerQueue = (logSettings.timer) ? new List<string>() : null;
        C_GlobalData.writeXML<LogSetting>(logDir + @"\LogSetting.xml", logSettings);
    }

    /// <summary> システム終了時の処理、ログを一気に書き込む。 </summary>
    static public void End()
    {
        try
        {

            // ログ保存先のディレクトリの設定 （サーバー側のフォルダ￥debug￥現在年月￥）
            String dir = C_GlobalData.GetPictureDir() + @"\debug\" + DateTime.Now.ToString("yyyyMM");
            if (Directory.Exists(dir) == false) Directory.CreateDirectory(dir);


            // 開始時刻・終了時刻・システムのバージョンの保存
            string text = beginTime.ToString("MM/dd HH:mm:ss") + "～" + DateTime.Now.ToString("MM/dd HH:mm:ss") + "," +
                          string.Join(",", C_GlobalData.GetIniData()) +
                          ",ﾊﾞｰｼﾞｮﾝ：" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version + System.IO.File.GetLastWriteTime(System.Reflection.Assembly.GetExecutingAssembly().Location).ToString("(yyyy-MM-dd HH:mm:ss)");
            File.AppendAllText(C_GlobalData.GetPictureDir() + @"\debug\起動ログ-" +
                               Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetExecutingAssembly().Location) + "-" +
                               System.Environment.MachineName + ".txt",
                               text + "\r\n",
                               Encoding.UTF8);

            // ログ保存の設定一覧 q:ログデータキュー（Stringのリスト）、s:ログのファイル名に付加する文字列
            var writeSettings = new[] 
            {
                new {q = logQueue, s = "-log-"},
                new {q = qrlogQueue, s = "-qrlog-"},
                new {q = qrerrQueue, s = "-qrerr-"},
                new {q = errQueue, s = "-err-"},
                new {q = timerQueue, s = "-timer-"},
            };



            foreach (var w in writeSettings)
            {
                // ログ保存先のキューがある場合、ファイルに保存する。
                if (w.q != null && w.q.Count > 0)
                {
                    // 保存先：このexeファイルの名称 -log- (ファイル名に付加する文字列) 現在日  PC名
                    String fn = Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetExecutingAssembly().Location) +
                                w.s + DateTime.Now.ToString("yyyyMMdd") + "-" + System.Environment.MachineName + ".txt";

                    File.AppendAllLines(dir + @"\" + fn, w.q.ToArray());
                }
            }
        }
        catch (Exception ex)
        {
            System.Diagnostics.Trace.WriteLine(ex.StackTrace);
        }
    }



    /// <summary>
    /// ログの保存（キューに保存する）
    /// </summary>
    /// <param name="text"></param>
    /// <param name="outQueue"></param>
    static private void write(string text, List<string> outQueue)
    {
        text = "[" + DateTime.Now.ToString("yyyyMMdd-HHmmss") + "]" + text;
        System.Diagnostics.Trace.WriteLine(text);
        if (outQueue != null)
        {
            lock (outQueue)
            {
                outQueue.Add(text);
            }
        }
    }


    /// <summary> ログ出力</summary>
    /// <param name="log">ログテキスト</param>
    static public void writeLog(String log)
    {
        write(log, logQueue);
    }

    /// <summary> QR読み込みログ出力</summary>
    /// <param name="log">ログテキスト</param>
    static public void writeQrLog(String log)
    {
        write(log, qrlogQueue);
    }


    /// <summary> QRエラーログ出力</summary>
    /// <param name="log">ログテキスト</param>
    static public void writeQrErr(String log)
    {
        write(log, qrerrQueue);
    }

    /// <summary> エラーログ出力</summary>
    /// <param name="log">ログテキスト</param>
    /// <param name="dlg">ポップアップの有無</param>
    static public void writeErr(String log, bool dlg)
    {
        if (dlg) MessageBox.Show(log, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
        write(log, errQueue);
    }

    /// <summary> エラーログ出力</summary>
    /// <param name="ex">例外</param>
    /// <param name="dlg">ポップアップの有無</param>
    static public void writeErr(Exception ex, bool dlg)
    {
        if (dlg) MessageBox.Show(ex.Message, "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
        write(ex.Message + "\r\n" + ex.StackTrace, errQueue);
    }


    /// <summary> タイマーログの開始 </summary>
    /// <returns>タイマーオブジェクト</returns>
    public static System.Diagnostics.Stopwatch BeginTimer()
    {
        return (timerQueue != null ? System.Diagnostics.Stopwatch.StartNew() : null);
    }

    /// <summary>タイマーログの出力 </summary>
    /// <param name="text">テキスト</param>
    /// <returns>タイマーオブジェクト</returns>
    public static void WriteTimer(System.Diagnostics.Stopwatch timer, String text)
    {
        if (timer != null)
        {
            timer.Stop();
            write(timer.ElapsedMilliseconds.ToString("0.0") + "msec  " + text, timerQueue);
        }
    }

}
