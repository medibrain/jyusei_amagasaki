﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace AmaInput
{
    public partial class FormOutput : Form
    {
        public FormOutput()
        {
            InitializeComponent();
        }

        private void FormOutput_Load(object sender, EventArgs e)
        {
            checkBoxNoOutputOnly.Checked = true;
        }



        /// <summary> 全選択のON・OFFのチェックの変更時の処理 </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            if (dataGridView1.Columns.Contains("chk"))
            {
                using (var dt = dataGridView1.DataSource as DataTable)
                {
                    foreach (DataRow row in dt.Rows) row["chk"] = checkBoxSelectAll.Checked;
                }
            }
        }

        /// <summary> 未出力のみのON・OFFのチェックの変更時の処理 </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxNoOutputOnly_CheckedChanged(object sender, EventArgs e)
        {
            UpdateView();
        }

        void UpdateView()
        {
            dataGridView1.DataSource = null;        // ビューのクリア
            checkBoxSelectAll.Checked = false;      // 全選択のチェックを解除

            string query = "SELECT gname, count(*) AS CNT, SUM(nooutput) AS NON FROM receipts WHERE rid in (SELECT 処理番号 FROM tdata) GROUP BY gname " +
                           " HAVING SUM(noninput) = 0 AND SUM(nocheck) = 0";
            if (checkBoxNoOutputOnly.Checked) query += " AND SUM(nooutput) > 0";
            query += " ORDER BY LEFT(gname,5) DESC, gname ";

            using (var dt = new DataTable())
            using (var da = SQLClass.GetDbDataAdapter(query))
            {
                dt.Columns.Add("chk", typeof(bool));
                dt.Columns["chk"].DefaultValue = false;
                da.Fill(dt);
                dataGridView1.DataSource = dt;
            }

            dataGridView1.Columns["chk"].HeaderText = "";
            dataGridView1.Columns["gname"].HeaderText = "スキャン束";
            dataGridView1.Columns["CNT"].HeaderText = "枚数";
            dataGridView1.Columns["NON"].HeaderText = "未出力";

            dataGridView1.Columns["gname"].ReadOnly = dataGridView1.Columns["CNT"].ReadOnly = dataGridView1.Columns["NON"].ReadOnly = true;

            dataGridView1.Columns["gname"].HeaderCell.Style.WrapMode =
            dataGridView1.Columns["CNT"].HeaderCell.Style.WrapMode =
            dataGridView1.Columns["NON"].HeaderCell.Style.WrapMode = DataGridViewTriState.False;

            dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);
            dataGridView1.Columns["CHK"].Width += 20;
        }

        /// <summary> 画像出力 </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (backgroundWorker1.IsBusy)
                {
                    backgroundWorker1.CancelAsync();
                    return;
                }


                int totalCount = 0;                         // 画像全枚数
                var groupList = new HashSet<string>();      // スキャンの束一覧

                using (var dt = dataGridView1.DataSource as DataTable)
                {
                    if (dt == null) return;

                    // 枚数チェック
                    foreach (DataRow row in dt.Rows)
                    {
                        if ((bool)row["chk"] == true)
                        {
                            totalCount += Convert.ToInt32(row["CNT"]);
                            groupList.Add(row["gname"].ToString());
                        }
                    }
                    if (totalCount <= 0) return;

                    if (MessageBox.Show("出力確認\r\n" + totalCount + "枚です。出力しますか？", "出力確認", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
                }


                button1.Text = "キャンセル";
                if (dataGridView1.Columns.Contains("chk")) dataGridView1.Columns["chk"].ReadOnly = true;
                checkBoxNoOutputOnly.Enabled = checkBoxSelectAll.Enabled = false;


                progressBar1.Visible = true;
                backgroundWorker1.RunWorkerAsync(groupList);


            }
            catch (Exception ex)
            {
                Logs.writeErr("エラーが発生しました、", true);
                Logs.writeErr(ex, false);
            }
        }



        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                
                e.Cancel = true;

                var groupList = e.Argument as HashSet<string>;
                using (var dt = new DataTable())
                {
                    // 該当データ一覧を取得する。
                    using (var da = SQLClass.GetDbDataAdapter("SELECT * FROM receipts, tdata WHERE 処理番号 = rid AND gname = :gname ORDER BY 束番号, 子番号"))
                    {
                        da.SelectCommand.Parameters.Add("gname", NpgsqlTypes.NpgsqlDbType.Text);
                        foreach (var item in groupList)
                        {
                            if (backgroundWorker1.CancellationPending) return;

                            da.SelectCommand.Parameters["gname"].Value = item;
                            da.Fill(dt);
                        }
                    }

                    // 出力ファイル名の設定
                    string prevs = "";              // 前画像の出力ファイル名
                    int z = 1;                      // 続紙番号
                    dt.Columns.Add("outfn");
                    foreach (DataRow r in dt.Rows)
                    {
                        if (backgroundWorker1.CancellationPending) return;

                        //// ファイルに付加する 診療年月 yy-mm の設定（スキャン時に設定した画像グループ名）
                        //string s = r["gname"].ToString().Substring(0, 5);

                        // ファイルに付加する 診療年月 yy-mm の設定（国保連データの審査月の前月）
                        int y, m;
                        if (int.TryParse(r["審査年"].ToString(), out y) == false) y = 0;
                        if (int.TryParse(r["審査月"].ToString(), out m) == false) m = 0;
                        if (y > 0 || m > 0)
                        {
                            m--;
                            if (m <= 0)
                            {
                                y--;
                                m += 12;
                            }
                        }
                        else
                        {
                            y = m = 0;
                        }
                        string s = y.ToString("00") + "-" + m.ToString("00");


                        // 施療年月 - 束番号 - 子番号 - 氏名
                        s += "-" + r["束番号"] + "-" + r["子番号"] + "-" + r["受診者氏名"];

                        if (s != prevs)
                        {
                            z = 1;
                            prevs = s;
                            r["outfn"] = s + ".tif";
                        }
                        else
                        {
                            z++;
                            r["outfn"] = s + "★" + z.ToString() + ".tif";
                        }
                    }


                    // CSVの出力 (UTF-8 BOM有り カンマ区切り)
                    var csvData = new string[dt.Rows.Count + 1];
                    var cells = new[] {
                                "gname",            // gname : 画像グループ名
                                "fname",            // fname : 出力ファイル名 　[診療年]-[診療月]-[束番号]-[子番号番号]-[氏名].tif
                                "審査年",
                                "審査月",
                                "束番号",
                                "子番号",
                                "施療年月",
                                "処理番号",
                                "受診者氏名",
                                "被保険者番号",
                                "生年月日",         // [生年月日年号][生年月日年]年[生年月日月]月[生年月日日]日
                                "性別",
                                "保険者番号",
                                "保険者名",
                                "機関番号",
                                "機関名",
                                "決定金額",
                                "住民コード",
                                "保険者ページ数",
                                "総ページ数",
                                "免除",
                                "公費負担者番号",
                                "負担区分",
                                "施療開始日",
                                "複数月区分",
                                "修正区分",
                                "受給者番号",
                                "負担割合",
                                "負傷年月日",
                                "施療終了日",
                                "実日数",
                                "一部負担金",
                                "備考1",
                                "備考2",
                            };
                    csvData[0] = string.Join(",", cells);

                    for (int n = 0; n < dt.Rows.Count; n++)
                    {

                        if (backgroundWorker1.CancellationPending) return;

                        DataRow r = dt.Rows[n];

                        int i = 0;
                        cells[i++] = r["gname"].ToString();     // gname : 画像グループ名
                        cells[i++] = r["outfn"].ToString();     // fname : 出力ファイル名  
                        cells[i++] = r["審査年"].ToString();
                        cells[i++] = r["審査月"].ToString();
                        cells[i++] = r["束番号"].ToString();
                        cells[i++] = r["子番号"].ToString();
                        cells[i++] = r["施療年月"].ToString();
                        cells[i++] = r["処理番号"].ToString();
                        cells[i++] = r["受診者氏名"].ToString();
                        cells[i++] = r["被保険者番号"].ToString();
                        cells[i++] = r["生年月日年号"].ToString() + r["生年月日年"] + "年" + r["生年月日月"] + "月" + r["生年月日日"] + "日";
                        cells[i++] = r["性別"].ToString();
                        cells[i++] = r["保険者番号"].ToString();
                        cells[i++] = r["保険者名"].ToString();
                        cells[i++] = r["機関番号"].ToString();
                        cells[i++] = r["機関名"].ToString();
                        cells[i++] = r["決定金額"].ToString();
                        cells[i++] = r["住民コード"].ToString();
                        cells[i++] = r["保険者ページ数"].ToString();
                        cells[i++] = r["総ページ数"].ToString();
                        cells[i++] = r["免除"].ToString();
                        cells[i++] = r["公費負担者番号"].ToString();
                        cells[i++] = r["負担区分"].ToString();
                        cells[i++] = r["施療開始日"].ToString();
                        cells[i++] = r["複数月区分"].ToString();
                        cells[i++] = r["修正区分"].ToString();
                        cells[i++] = r["受給者番号"].ToString();
                        cells[i++] = r["負担割合"].ToString();
                        cells[i++] = r["負傷年月日"].ToString();
                        cells[i++] = r["施療終了日"].ToString();
                        cells[i++] = r["実日数"].ToString();
                        cells[i++] = r["一部負担金"].ToString();
                        cells[i++] = r["備考1"].ToString();
                        cells[i++] = r["備考2"].ToString();

                        csvData[1 + n] = string.Join(",", cells);

                    }



                    // ルートフォルダ
                    string root = System.Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) +
                                  "\\尼崎柔整出力\\" + DateTime.Now.ToString("yyyyMMdd-HHmmss");


                    Directory.CreateDirectory(root);
                    File.WriteAllLines(root + "\\juusei.csv", csvData, Encoding.UTF8);


                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        backgroundWorker1.ReportProgress(i * 90 / dt.Rows.Count);

                        DataRow row = dt.Rows[i];

                        if (backgroundWorker1.CancellationPending) return;

                        string fromPath = C_GlobalData.GetPictureDir() + "\\" + row["gname"] + "\\" + row["fname"];
                        string destPath = root + "\\" + row["gname"];
                        if (Directory.Exists(destPath) == false) Directory.CreateDirectory(destPath);

                        destPath  += "\\" + row["outfn"];
                        File.Copy(fromPath, destPath);
                    }

                    using (var cmd = SQLClass.CreateCommand("UPDATE receipts SET nooutput = 0 WHERE gname = :g AND fname = :f"))
                    {
                        cmd.Parameters.Add("g", NpgsqlTypes.NpgsqlDbType.Text);
                        cmd.Parameters.Add("f", NpgsqlTypes.NpgsqlDbType.Text);


                        for (int i = 0; i < dt.Rows.Count; i++)
                        {

                            backgroundWorker1.ReportProgress(90 + i * 10 / dt.Rows.Count);

                            cmd.Parameters["g"].Value = dt.Rows[i]["gname"];
                            cmd.Parameters["f"].Value = dt.Rows[i]["fname"];
                            cmd.ExecuteNonQuery();
                        }

                    }
                

                    backgroundWorker1.ReportProgress(100);    
                    System.Diagnostics.Process.Start(root);
                }



                e.Result = null;
                e.Cancel = false;
            }
            catch (Exception ex)
            {
                e.Result = ex;
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
            }
            else if ((e.Result as Exception) != null)
            {
                Logs.writeErr("エラーが発生しました。", true);
                Logs.writeErr((e.Result as Exception), false);
            }
            else 
            {
                MessageBox.Show("とり込み完了");
            }
            

            UpdateView();

            button1.Text = "画像出力";


            if (dataGridView1.Columns.Contains("chk")) dataGridView1.Columns["chk"].ReadOnly = false;
            checkBoxNoOutputOnly.Enabled = checkBoxSelectAll.Enabled = true;
            progressBar1.Visible = false;
        }
    }
}

