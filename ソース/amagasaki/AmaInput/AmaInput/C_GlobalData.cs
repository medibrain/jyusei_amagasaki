﻿using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.Collections.Generic;
using System.Collections;
using System.Threading;
using System.Data.Common;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;


/// <summary>
/// グローバルなデータを格納するクラス
/// </summary>
public class C_GlobalData
{
    /// <summary>
    /// 自分自身のアプリのパスを取得する
    /// </summary>
    static public string getAppDir()
    {
        string path = System.Reflection.Assembly.GetEntryAssembly().Location;
        return Path.GetDirectoryName(path);
    }

    /// <summary> INIファイル情報保存用 </summary>    
    static List<string> iniData = null;

    /// <summary> INIファイル情報を読み込む </summary>
    /// <returns></returns>
    public static string[] GetIniData()
    {
        if (iniData == null)
        {            
            iniData = new List<string>();
            string[] file = File.ReadAllLines(getAppDir() + @"\SQL.ini");
            foreach (var line in file)
            {
                /// 空白、 ; で始まるコメント行 以外を読み込む
                if (line.Trim().Length > 0 && line.Trim()[0] != ';') iniData.Add(line);
            }
        }   
        return iniData.ToArray();
    }

    /// <summary> スキャナフォルダ の取得</summary>
    /// <returns></returns>
    public static string GetScanDir()
    {
        // INIファイルの1行目 但し * → EXEと同じ
        return GetIniData()[0].Trim();
    }


    /// <summary> 画像ファイル、SQLight保存用フォルダ の取得</summary>
    /// <returns></returns>
    public static string GetPictureDir()
    {
        // INIファイルの1行目 但し * → EXEと同じ
        return GetIniData()[1].Replace("*", getAppDir()).Trim();
    }

    /// <summary> PostgreSql用ConnectTextの取得</summary>
    /// <returns></returns>
    public static string ConnectText()
    {
        string[] l = GetIniData();
        string s = "";
        for (int i = 2; i < l.Length; i++) s += l[i] + "\r\n";

        return s;
    }

    /// <summary>
    /// コンボボックスの値の変更時の関数をセットする
    /// </summary>
    /// <param name="c">コンボボックス</param>
    /// <param name="millsec">タイマー(ミリ秒)</param>
    /// <param name="func">関数</param>
    public static void SetComboChangedFunc(ComboBox c, int millsec, Func<ComboBox, bool> func)
    {
        // コンボボックスの変更時の関数をセットする。
        c.SelectedValueChanged += new System.EventHandler((sender, e) =>
        {
            try
            {
                // コンボボックスのテキスト
                var t = (c as ComboBox).Text;
                
                // 待機スレッド起動
                new System.Threading.Thread(() =>
                {
                    try
                    {
                        System.Threading.Thread.Sleep(millsec);
                        // スレッド開始時の選択と比べ、変更がないことを確認する。
                        c.Invoke((MethodInvoker)(() =>
                        {
                            try
                            {
                                if (t == c.Text)
                                {
                                    func(c);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logs.writeErr(ex, false);
                            }
                        }));
                    }
                    catch (Exception ex)
                    {
                        Logs.writeErr(ex, false);
                    }
                }
                ).Start();
            }
            catch (Exception ex)
            {
                Logs.writeErr(ex, false);
            }
        });
    }


    /// <summary>ソート用クラス</summary>
    private class C_SortObject : IComparable
    {
        /// <summary>ソートしたいデータ</summary>
        public object val;
        /// <summary>ソート順序</summary>
        public int order;

        public int CompareTo(object obj)
        {
            var s = obj as C_SortObject;
            if (s == null) return 0;
            else return order - s.order;
        }
    }


    /// <summary>DataGridViewから、選択中のセルをクリップボードにコピーする。</summary>
    /// <param name="view"></param>
    public static void DataGridView_CripbordCopy_SelectedCells(System.Windows.Forms.DataGridView view)
    {
        // ヘッダをコピー出来ないようにする
        view.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
        var value = view.GetClipboardContent();
        if (value != null) System.Windows.Forms.Clipboard.SetDataObject(value);
    }

    /// <summary>DataGridView全体をクリップボードにコピーする。</summary>
    /// <param name="view"></param>
    private static void DataGridView_CripbordCopy_All(System.Windows.Forms.DataGridView view)
    {
        var col = new List<C_SortObject>();
        foreach (System.Windows.Forms.DataGridViewColumn item in view.Columns)
        {
            if (item.Visible) col.Add(new C_SortObject() { val = item.Name, order = item.DisplayIndex });
        }
        col.Sort();


        var csvrows = new List<string>();
        var csvcells = new List<string>();


        foreach (var item in col) csvcells.Add(view.Columns[item.val.ToString()].HeaderText.Replace("\t", " ").Replace("\"", ""));
        csvrows.Add("\"" + string.Join("\"\t\"", csvcells.ToArray()) + "\"");
        csvcells.Clear();

        for (int y = 0; y < view.Rows.Count; y++)
        {

            foreach (var item in col) csvcells.Add(view[item.val.ToString(), y].FormattedValue.ToString().Replace("\t", " ").Replace("\"", ""));
            csvrows.Add("\"" + string.Join("\"\t\"", csvcells.ToArray()) + "\"");
            csvcells.Clear();
        }
        System.Windows.Forms.Clipboard.SetDataObject(string.Join("\r\n", csvrows));
    }





    /// <summary>DataGeidViewにコピーボタンを追加する</summary>
    /// <param name="view"></param>
    public static void DataGridView_AddCripbordCopyMenu(System.Windows.Forms.DataGridView view)
    {
        try
        {
            var h = new EventHandler((o, a) =>
            {
                try
                {
                    var i = o as System.Windows.Forms.ToolStripMenuItem;
                    if (i == null) return;

                    var m = i.Owner as System.Windows.Forms.ContextMenuStrip;
                    if (m == null) return;

                    var v = m.SourceControl as System.Windows.Forms.DataGridView;
                    if (v == null) return;

                    if (i.Text == "コピー(選択中)") DataGridView_CripbordCopy_SelectedCells(v);
                    else if (i.Text == "コピー(全件)") DataGridView_CripbordCopy_All(v);
                }
                catch (Exception ex)
                {
                    Logs.writeErr(ex, false);
                }
            });


            if (view.ContextMenuStrip == null) view.ContextMenuStrip = new System.Windows.Forms.ContextMenuStrip();
            if (view.ContextMenuStrip.Items.ContainsKey("コピー(選択中)") == false) view.ContextMenuStrip.Items.Add("コピー(選択中)", null, h);
            if (view.ContextMenuStrip.Items.ContainsKey("コピー(全件)") == false) view.ContextMenuStrip.Items.Add("コピー(全件)", null, h);
        }
        catch (Exception ex)
        {
            Logs.writeErr(ex, false);
        }
    }

    /// <summary>
    /// XMLの読み込み
    /// </summary>
    /// <typeparam name="T">型</typeparam>
    /// <param name="path">XMLファイルのパス</param>
    /// <returns>読込結果（読み込み失敗時はデフォルト値 new() を返す！）</returns>
    static public T readXML<T>(string path) where T : new()
    {
        try
        {
            if(File.Exists(path) == false) 
            {
                T t = new T();
#if DEBUG
                writeXML<T>(path, t);
#endif
                return t;
            }

            //読み込むファイルを開く
            using (var sr = new System.IO.StreamReader(path, new System.Text.UTF8Encoding(false)))
            {
                var l = new List<string>();
                l.Add("");

                //XmlSerializerオブジェクトを作成
                var serializer = new System.Xml.Serialization.XmlSerializer(typeof(T));
                //XMLファイルから読み込み、逆シリアル化する
                var o = (T)serializer.Deserialize(sr);
                //ファイルを閉じる
                sr.Close();
                return o;
            }
        }
        catch (Exception ex)
        {
            Logs.writeErr(ex, false); 
            return new T();
        }
    }


    static public void writeXML<T>(string path, T data) where T : new()
    {
        try
        {
            //XmlSerializerオブジェクトを作成 オブジェクトの型を指定する
            var serializer = new System.Xml.Serialization.XmlSerializer(typeof(T));
            //書き込むファイルを開く（UTF-8 BOM無し）
            using (var sw = new System.IO.StreamWriter(path, false, new System.Text.UTF8Encoding(false)))
            {
                //シリアル化し、XMLファイルに保存する
                serializer.Serialize(sw, data);
                //ファイルを閉じる
                sw.Close();
            }
        }
        catch (Exception ex)
        {
            Logs.writeErr(ex, false);
        }
    }

    /// <summary>
    /// 画像を所定のフォーマットで保存する。
    /// </summary>
    /// <param name="img">画像</param>
    /// <param name="path">パス</param>
    /// <param name="mimeType">例：image/tiff</param>
    /// <param name="encoderValue">例：CCITT4</param>
    static public void writeImage(System.Drawing.Image img, string path, string mimeType, System.Drawing.Imaging.EncoderValue encoderValue)
    {
        // エンコードの取得
        System.Drawing.Imaging.ImageCodecInfo encode = null;
        foreach (var item in System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders())
        {
            if (item.MimeType == mimeType)
            {
                encode = item;
                break;
            }
        }

        var myEncoder = System.Drawing.Imaging.Encoder.Compression;
        var myEncoderParameters = new System.Drawing.Imaging.EncoderParameters(1);
        var myEncoderParameter = new System.Drawing.Imaging.EncoderParameter(myEncoder,　(long) encoderValue);
        myEncoderParameters.Param[0] = myEncoderParameter;
        img.Save(path, encode, myEncoderParameters);
    }

    /// <summary>
    /// CCITT4のTIFF形式で画像を保存する
    /// </summary>
    /// <param name="img">画像</param>
    /// <param name="path">パス</param>
    static public void writeCCITT4(System.Drawing.Image img, string path)
    {
        writeImage(img, path, "image/tiff", System.Drawing.Imaging.EncoderValue.CompressionCCITT4);
    }



}
