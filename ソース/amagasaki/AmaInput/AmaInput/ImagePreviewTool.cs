﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows;
using System.Windows.Forms;

namespace AmaInput
{
    public class ImagePreviewTool
    {
        UserControlImage[] pictureBoxList;

        SplitContainer[] splitContainerList;

        public class InputWindowSize
        {
            public Point[] PictureBoxPos = null;
            public Size[] PictureBoxSize = null;
            public int[] splitDistance = null;
        }

        string xml;

        string imgpath;

        Image img;

        public void Init(string xml, UserControlImage[] pictureBoxList, SplitContainer[] splitContainerList)
        {
            this.xml = xml;
            this.pictureBoxList = pictureBoxList;
            this.splitContainerList = splitContainerList;

            try
            {
                InputWindowSize setting = C_GlobalData.readXML<InputWindowSize>(xml);
                for (int i = 0; i < setting.PictureBoxPos.Length && i< setting.PictureBoxSize.Length  && i <  pictureBoxList.Length;  i++)
                {
                    pictureBoxList[i].SetPictureBoxLocation(setting.PictureBoxSize[i], setting.PictureBoxPos[i]);
                }

                for (int i = 0; i < setting.splitDistance.Length && i < splitContainerList.Length; i++)
                {
                    splitContainerList[i].SplitterDistance = setting.splitDistance[i];
                }
            }
            catch (Exception ex)
            {
                Logs.writeErr(ex, false);
            }
        }

        public void Exit()
        {
            DisposeImage();
            try
            {
                InputWindowSize setting = new InputWindowSize();
                setting.PictureBoxPos = new Point[pictureBoxList.Length];
                setting.PictureBoxSize = new Size[pictureBoxList.Length];
                setting.splitDistance = new int[splitContainerList.Length];


                for (int i = 0; i < pictureBoxList.Length; i++)
                {
                    pictureBoxList[i].GetPictureBoxLocation(out setting.PictureBoxSize[i], out setting.PictureBoxPos[i]);
                }

                for (int i = 0; i < splitContainerList.Length; i++)
                {
                    setting.splitDistance[i] = splitContainerList[i].SplitterDistance;
                }
                C_GlobalData.writeXML<InputWindowSize>(xml, setting);
            }
            catch (Exception ex)
            {
                Logs.writeErr(ex, false);
            }

        }

        public void ShowImage(string imgpath)
        {
            DisposeImage();

            try
            {
                if (System.IO.File.Exists(imgpath))
                {
                    this.img = Image.FromFile(imgpath);
                    foreach (var item in pictureBoxList) item.Image = this.img;
                    this.imgpath = imgpath;
                }
            }
            catch (Exception ex)
            {
                Logs.writeErr(ex, false);
            }
        }
        
        public void RotateImage(bool clockwise)
        {
            try
            {
                if (img != null)
                {
                    img.RotateFlip(clockwise ? RotateFlipType.Rotate90FlipNone : RotateFlipType.Rotate270FlipNone);
                    foreach (var item in pictureBoxList) item.Refresh();
                    C_GlobalData.writeCCITT4(img, imgpath);
                }
            }
            catch (Exception ex)
            {
                Logs.writeErr(ex, false);
            }
        }

        private void DisposeImage()
        {
            try
            {
                foreach (var item in pictureBoxList) item.Image = null;
                if (img != null) img.Dispose();
            }
            catch (Exception ex)
            {
                Logs.writeErr(ex, false);
            }

            this.img = null;
            this.imgpath = "";
        }
    }
}
