﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.VisualBasic.FileIO;

namespace AmaInput
{
    public partial class ReadCsvForm : Form
    {
        /// <summary> 保存先テーブル名（tdata）</summary>
        const string TDATA = "tdata";

        /// <summary> キー列 処理番号 </summary>
        const string KEY_COL = "処理番号";


        /// <summary> コンストラクタ・読み込み対象のファイルを設定する </summary>
        /// <param name="files">読み込み対象のファイル</param>
        public ReadCsvForm(params string[] files)
        {
            InitializeComponent();
            progressBar1.Maximum = files.Length * 100;
            backgroundWorker1.RunWorkerAsync(files);
        }

        /// <summary> キャンセルボタン → Close </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary> Close処理 バックグラウンド実行時は中断・実行時以外はクローズ </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReadCsvForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (backgroundWorker1.IsBusy)
            {
                backgroundWorker1.CancelAsync();
                e.Cancel = true;
            }
            else
            {
                e.Cancel = false;
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            e.Cancel = true;
            string[] files = e.Argument as string[];

            try
            {
                for (int i = 0; i < files.Length; i++)
                {
                    ReadCsv(files[i], i * 100);
                    if (backgroundWorker1.CancellationPending) return;
                }

            }
            catch (Exception ex)
            {
                Logs.writeErr(ex, false);
                e.Result = ex;
            }

            e.Cancel = false;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {            
            if (e.Cancelled)
            {
                this.Close();
            }
            else if (e.Result != null)
            {
                label1.BackColor = Color.Red;
                label1.ForeColor = Color.White;
                label1.Text = "エラーが発生しました";
                buttonCancel.Text = "OK";
            }
            else
            {
                progressBar1.Value = progressBar1.Maximum;
                label1.BackColor = Color.Blue;
                label1.ForeColor = Color.White;
                label1.Text = "取り込み完了";
                buttonCancel.Text = "OK";
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }



        /// <summary> CSVの取込＋DBへの登録</summary>
        /// <param name="pathName">CSVファイル名（フルパス名）</param>
        /// <param name="progMin">プログレスのMIN</param>
        void ReadCsv(string pathName, int progMin)
        {
            // プログレスバーの設定
            backgroundWorker1.ReportProgress(progMin);

            using (var dt = new DataTable())
            using (var da = SQLClass.GetDbDataAdapter("SELECT * FROM " + TDATA + " LIMIT 0"))
            {
                // DataTableに、DBの列名一覧を格納する。
                da.Fill(dt);

                // CSVを読み込む
                ReadCsv(pathName, dt);

                // 8％進める。
                progMin += 8;
                backgroundWorker1.ReportProgress(progMin);

                var names = new string[dt.Columns.Count];         // DBに含まれている列名リスト
                var updColumns = new string[dt.Columns.Count];    // 列名 = :列名 リスト
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    names[i] = dt.Columns[i].ColumnName;
                    updColumns[i] = names[i] + "=:" + names[i];
                }


                // ここで、束番号と子番号を付加する！
                int tabaNo = 801;
                int koNo = 0;
                foreach (DataRow row in dt.Rows)
                {
                    koNo++;

                    if (row["保険者番号"].ToString() == "67280032")
                    {
                        // 尼崎後期高齢の場合。束番号は850から始まる。
                        if (tabaNo < 850)
                        {
                            // 850未満の場合、束番号＝850、子番号＝001 でリセットする
                            tabaNo = 850;
                            koNo = 1;
                        }
                    }
                    else
                    {
                        // 尼崎後期高齢の場合。束番号は801からの連番。子は1～390
                        if (koNo > 390)
                        {
                            // 子番号が390の次は、束番号を繰り上げ、子番号をリセットする。、
                            koNo = 1;
                            tabaNo++;
                        }
                    }


                    row["束番号"] = tabaNo.ToString("000");
                    row["子番号"] = koNo.ToString("000");

                    if (backgroundWorker1.CancellationPending) return;

                }


                // 2％進める。
                progMin += 2;
                backgroundWorker1.ReportProgress(progMin);


                string queryCnt = "SELECT COUNT(*) FROM " + TDATA + " WHERE " + KEY_COL + " = :" + KEY_COL;
                string queryUpd = "UPDATE " + TDATA + " SET " + string.Join(",", updColumns) + " WHERE " + KEY_COL + " = :" + KEY_COL;
                string queryIns = "INSERT INTO " + TDATA + " (" + string.Join(",", names) + ") VALUES (:" + string.Join(",:", names) + ")";

                using (var cmdCnt = SQLClass.CreateCommand(queryCnt))
                using (var cmdUpd = SQLClass.CreateCommand(queryUpd))
                using (var cmdIns = SQLClass.CreateCommand(queryIns))
                {
                    foreach (var name in names)
                    {
                        cmdCnt.Parameters.Add(name, NpgsqlTypes.NpgsqlDbType.Text);
                        cmdUpd.Parameters.Add(name, NpgsqlTypes.NpgsqlDbType.Text);
                        cmdIns.Parameters.Add(name, NpgsqlTypes.NpgsqlDbType.Text);
                    }


                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (backgroundWorker1.CancellationPending) return;

                        backgroundWorker1.ReportProgress(progMin + 90 * i / dt.Rows.Count);

                        DataRow row = dt.Rows[i];

                        row["施療年月"] = row["施療年"].ToString().PadLeft(2, '0') + row["施療月"].ToString().PadLeft(2, '0');

                        row["決定金額"]= row["決定金額"].ToString().Replace(",","");


                        foreach (var name in names)
                        {
                            cmdCnt.Parameters[name].Value = cmdUpd.Parameters[name].Value = cmdIns.Parameters[name].Value = row[name];
                        }

                        // cmdCnt の実行
                        int cnt;

                        using (var r = cmdCnt.ExecuteReader())
                        {
                            r.Read();
                            cnt = Convert.ToInt32(r[0]);
                            r.Close();
                        }


                        if (cnt > 0)
                        {
                            // cmdUpdの実行
                            cmdUpd.ExecuteNonQuery();
                        }
                        else
                        {
                            // cmdInsの実行
                            cmdIns.ExecuteNonQuery();
                        }
                    }
                }
            }
        }
    



        /// <summary> CSVファイルのREAD </summary>
        /// <param name="path">CSVファイル</param>
        /// <param name="dt">取り込み先データテーブル（列は設定済み）</param>
        void ReadCsv(string path, DataTable dt)
        {
            using (var parser = new TextFieldParser(path, System.Text.Encoding.GetEncoding("Shift_JIS")))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");                      // 区切り文字はコンマ

                // 先頭行の読み込み （列名リスト）
                int key_colnum = -1;

                var columnList = new List<string>();
                if (!parser.EndOfData)
                {
                    string[] cells = parser.ReadFields();       // 先頭行をREAD

                    for (int x = 0; x < cells.Length; x++)
                    {
                        string name = cells[x].Trim();
                        columnList.Add(name);
                        if (name == KEY_COL) key_colnum = x;

                    }
                }

                // 処理番号なし
                if (key_colnum < 0) return;

                while (!parser.EndOfData)
                {
                    string[] cells = parser.ReadFields();           // １行READ

                    if (cells.Length <= key_colnum || cells[key_colnum].Trim().Length <= 0) continue;


                    var row = dt.Rows.Add();                    // 出力先１行追加

                    for (int x = 0; x < Math.Min(cells.Length, columnList.Count); x++)
                    {
                        string colname = columnList[x];
                        if (colname != "")
                        {
                            row[colname] = cells[x].Trim();
                        }
                    }
                }
            }
        }


    }
}
