﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/// <summary>
/// 画像表示用のユーザーコントロール
/// </summary>
public partial class UserControlImage : UserControl
{
    /// <summary>
    /// マウスをホイールした時の拡大比率（２％）
    /// </summary>
    private const double HIRITSU_HEEL = 0.02;

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public UserControlImage()
    {
        InitializeComponent();

        /// ピクチャーコントロールのマウスイベントを設定する。
        // マウス移動
        pictureBox.MouseMove += new MouseEventHandler(PictureBoxMouseMove);
        // マウスアップ
        pictureBox.MouseUp += new MouseEventHandler(PictureBoxMouseUp);
        // マウスダウン
        pictureBox.MouseDown += new MouseEventHandler(PictureBoxMouseDown);
        // マウスホイール
        pictureBox.MouseWheel += new MouseEventHandler(PictureBoxMouseMouseWheel);
        // クリック
        pictureBox.Click += new EventHandler(PictureBoxMouseClick);
    }

    /// <summary>
    /// 初期化
    /// </summary>
    public void InitPictureBoxLocation()
    {
        // スクロール位置、サイズを初期設定する
        this.AutoScrollPosition = new Point(0, 0);
        pictureBox.Size = this.Size;
        pictureBox.Dock = DockStyle.Fill;
    }

    /// <summary>
    /// 表示する画像（PictureBox）のサイズ、位置（パネルのスクロール位置）を設定する
    /// </summary>
    /// <param name="size">表示する画像のサイズ</param>
    /// <param name="pos">位置</param>
    public void SetPictureBoxLocation(Size size, Point pos)
    {
        if (size.Height <= this.Height && size.Width <= this.Width)
        {
            /// 画像のサイズ(size) ≦ 枠のサイズの時(this.Size) → ドッキングさせる。完全に枠の大きさに合わせる！
            this.AutoScrollPosition = new Point(0, 0);  // 位置＝原点
            pictureBox.Size = this.Size;                // ピクチャーボックスのサイズ＝枠のサイズ
            pictureBox.Dock = DockStyle.Fill;           // ピクチャーボックスのドッキング
        }
        else
        {
            /// 画像のサイズ(size) ＞ 枠のサイズの時(this.Size) → ドッキングさせない。フレームよりピクチャーボックスが大きくなる。
            pictureBox.Dock = DockStyle.None;           // ドッキングを外す （スクロールバーが表示される）
            pictureBox.Size = size;                     // ピクチャーボックスのサイズを設定する
            this.AutoScrollPosition = pos;              // スクロールバーの位置を設定する。
        }
    }

    /// <summary>
    /// 表示画像（PictureBox）のサイズ、位置（パネルのスクロール位置）を取得する
    /// </summary>
    /// <param name="pictureSize"></param>
    /// <param name="sclool"></param>
    public void GetPictureBoxLocation(out Size pictureSize, out Point sclool)
    {
        if (pictureBox.Dock == DockStyle.Fill)
        {
            pictureSize = this.Size;
            sclool = new Point(0, 0);
        }
        else
        {
            pictureSize = pictureBox.Size;
            sclool = new Point(-this.AutoScrollPosition.X, -this.AutoScrollPosition.Y);
        }
    }


    /// <summary>
    /// マウスをホイールさせても、スクロールしない
    /// </summary>
    /// <param name="e"></param>
    protected override void OnMouseWheel(MouseEventArgs e)
    {
        // base.OnMouseWheel(e);
    }

    /// <summary>
    /// System.Windows.Forms.PictureBox によって表示されるイメージを取得します。
    /// </summary>
    public Image Image
    {
        get { return pictureBox.Image; }
        set { pictureBox.Image = value; }
    }

    /// <summary> System.Windows.Forms.PictureBox に表示するイメージのパスまはた URL を取得または設定します。</summary>
    public string ImageLocation
    {
        get
        {
            return pictureBox.ImageLocation;
        }
        set
        {
            try
            {
                // 元画像の削除
                if (pictureBox.Image != null) pictureBox.Image.Dispose();
            }
            catch (Exception)
            {
            }
            pictureBox.ImageLocation = "";
            pictureBox.ImageLocation = value;
        }
    }


    /// <summary> 左ボタンの状態 </summary>
    bool dragBtnL = false;

    /// <summary> 右ボタンの状態 </summary>
    bool dragBtnR = false;

    /// <summary> 中央ボタンの状態 </summary>
    bool dragBtnM = false;

    /// <summary> マウスボタン（左or右or中央）の状態 </summary>
    bool dragBtn { get { return dragBtnL || dragBtnM || dragBtnR; } }

    /// <summary> ピクチャーボックスのドラッグ位置（前回位置）、ドラッグ中止時は NULL </summary>
    private Point dragPosStart;

    /// <summary>
    /// マウスボタンを押した時の処理
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void PictureBoxMouseDown(object sender, MouseEventArgs e)
    {
        /// 左・右・中央ボタンの状態を保存する。 （false→trueに切り替える）
        if ((e.Button & MouseButtons.Left) == MouseButtons.Left) dragBtnL = true;       // 左ボタンの状態
        if ((e.Button & MouseButtons.Right) == MouseButtons.Right) dragBtnR = true;     // 右ボタンの状態
        if ((e.Button & MouseButtons.Middle) == MouseButtons.Middle) dragBtnM = true;   // 中央ボタンの状態

        if (dragBtn)
        {
            /// ドラッグ開始位置を更新する
            dragPosStart = e.Location;
            /// マウスカーソルをドラッグ中の変えるものに変える
            pictureBox.Cursor = System.Windows.Forms.Cursors.NoMove2D;
        }
    }

    /// <summary>
    /// マウスボタンを離した時の処理
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void PictureBoxMouseUp(object sender, MouseEventArgs e)
    {
        /// 左・右・中央ボタンの状態を保存する。 （true→falseに切り替える）
        if ((e.Button & MouseButtons.Left) == MouseButtons.Left) dragBtnL = false;      // 左ボタンの状態
        if ((e.Button & MouseButtons.Right) == MouseButtons.Right) dragBtnR = false;    // 右ボタンの状態
        if ((e.Button & MouseButtons.Middle) == MouseButtons.Middle) dragBtnM = false;  // 中央ボタンの状態

        if (dragBtn)
        {
            // 左・右・中央ボタンの何れかがまだ押されている状態の時
            /// ドラッグ開始位置を更新する
            dragPosStart = e.Location;
            /// マウスカーソルをドラッグ中の変えるものに変える
            pictureBox.Cursor = System.Windows.Forms.Cursors.NoMove2D;
        }
        else
        {
            /// マウスカーソルを標準のものに変える
            pictureBox.Cursor = System.Windows.Forms.Cursors.Default;
        }
    }

    /// <summary>
    /// マウス移動時の処理
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void PictureBoxMouseMove(object sender, MouseEventArgs e)
    {
        // ドラッグ中ならスクロール
        if (dragBtn)
        {
            Point pos = new Point(e.Location.X - dragPosStart.X, e.Location.Y - dragPosStart.Y);
            
            this.AutoScrollPosition = new Point(
                -this.AutoScrollPosition.X - pos.X,
                -this.AutoScrollPosition.Y - pos.Y);
        }
    }


    private void PictureBoxMouseClick(object sender, EventArgs e)
    {
        Invalidate(false);
        var pos = this.AutoScrollPosition;
        pictureBox.Select();
        this.AutoScrollPosition = new Point(-pos.X, -pos.Y);
    }

    private void PictureBoxMouseMouseWheel(object sender, MouseEventArgs e)
    {
        // マウス座標（Windows画面の左上が原点）
        var p = (sender as Control).PointToScreen(e.Location);

        // ピクチャーボックスを格納しているパネルの位置
        var panel = (sender as Control).Parent;
        var p1 = panel.PointToScreen(new Point(0, 0));

        // ドラッグの位置が、パネルの位置に収まっている場合のみ、ドラッグ処理を行う。
        if (p1.X < p.X && p.X < p1.X + panel.Width && p1.Y < p.Y && p.Y < p1.Y + panel.Height)
        {
            Invalidate(false);
            Zoom(e.Delta >= 1);
        }
    }

    /// <summary>
    /// ピクチャーボックスのズームの実行
    /// </summary>
    /// <param name="zoomUp">true:拡大、false:縮小</param>
    /// <see cref="HIRITSU_HEEL">拡大率</see>
    private void Zoom(bool zoomUp)
    {
        // 現在のスクロールの位置 ＝（スクロール位置 ÷ ピクチャーボックスのサイズ）
        double posX = -(double)this.AutoScrollPosition.X / (double)pictureBox.Width;
        double posY = -(double)this.AutoScrollPosition.Y / (double)pictureBox.Height;

        if (pictureBox.Image == null || pictureBox.Image.Width <= 0 || pictureBox.Image.Height <= 0) return;

        // 現在の比率＝ピクチャーボックスのサイズ ÷ 画像のサイズ 
        double rate = Math.Min((double)pictureBox.Width / (double)pictureBox.Image.Width, (double)pictureBox.Height / (double)pictureBox.Image.Height);

        if (zoomUp && rate >= 1) return;
        else if (pictureBox.Dock == DockStyle.Fill && zoomUp == false) return;

        rate += (zoomUp) ? HIRITSU_HEEL : -HIRITSU_HEEL;    // 上スクロール +10%  / 下スクロール +10%
        if (rate > 1) rate = 1;                             // 倍率は100%までとする。

        
        if (pictureBox.Width <= pictureBox.Parent.Width && pictureBox.Height <= pictureBox.Parent.Height)
        {
            pictureBox.Dock = DockStyle.Fill;
        }
        else
        {
            // スクロール位置 ÷ ピクチャーボックスのサイズが一定になるよう補正する！
            this.AutoScrollPosition = new Point((int)(posX * (double)pictureBox.Width), (int)(posY * (double)pictureBox.Height));
        }


        pictureBox.Dock = DockStyle.None;
        pictureBox.Width = (int)(rate * pictureBox.Image.Width);
        pictureBox.Height = (int)(rate * pictureBox.Image.Height);

        if (pictureBox.Width <= pictureBox.Parent.Width && pictureBox.Height <= pictureBox.Parent.Height)
        {
            pictureBox.Dock = DockStyle.Fill;
        }
        else
        {
            // スクロール位置 ÷ ピクチャーボックスのサイズが一定になるよう補正する！
            this.AutoScrollPosition = new Point((int)(posX * (double)pictureBox.Width), (int)(posY * (double)pictureBox.Height));
        }
    }
}
