﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AmaInput
{
    public partial class FormCheckFix : Form
    {

        ImagePreviewTool imagePreviewTool = new ImagePreviewTool();

        /// <summary> 審査月の取得 </summary>
        /// <param name="gname">スキャングループ名（診療年-診療月-束番号）</param>
        /// <returns>[0]審査年, [1]審査月, [2]束番号</returns>
        static private int[] GetBillMonth(string gname)
        {
            var s = gname.Split('-');
            int[] d = new int[3];
            d[0] = int.Parse(s[0]);         // 診療年
            d[1] = int.Parse(s[1]);         // 診療月
            d[2] = int.Parse(s[2]);         // 束番号

            d[1]++;                         // 審査月＝診療月の翌月
            if (d[1] > 12)                  // 13月を翌年１月に変換
            {
                d[1] -= 12;
                d[0]++;
            }
            return d;
            
        }

        /// <summary>
        /// データチェックの実行
        /// </summary>
        /// <param name="gname">グループ名 （yy-mm-ttt）</param>
        static public void DataCheck(string gname)
        {
            try
            {
                int[] bill = GetBillMonth(gname);   // 審査年、審査月、束番号を取得

                using(var dt1 = new DataTable())
                using(var da = SQLClass.GetDbDataAdapter("SELECT * FROM receipts WHERE gname = :gname ORDER BY fname"))
                using(da.UpdateCommand = SQLClass.CreateCommand("UPDATE receipts SET rid = :rid, nocheck = :nocheck WHERE gname = :gname AND fname = :fname"))
	            {
		            da.SelectCommand.Parameters.Add(":gname", NpgsqlTypes.NpgsqlDbType.Text).Value = gname;
                    

                    da.UpdateCommand.Parameters.Add(":rid", NpgsqlTypes.NpgsqlDbType.Text).SourceColumn = "rid";
                    da.UpdateCommand.Parameters.Add(":nocheck", NpgsqlTypes.NpgsqlDbType.Integer).SourceColumn = "nocheck";
                    da.UpdateCommand.Parameters.Add(":gname", NpgsqlTypes.NpgsqlDbType.Text).SourceColumn = "gname";
                    da.UpdateCommand.Parameters.Add(":fname", NpgsqlTypes.NpgsqlDbType.Text).SourceColumn = "fname";
                    
                    da.Fill(dt1);
                    dt1.AcceptChanges();


                    foreach (DataRow row in dt1.Rows)
	                {
		                if(Convert.ToInt32(row["noninput"]) == 0 && Convert.ToInt32(row["nocheck"]) != 0 && row["hiho"].ToString() == "*******")
                        {
                            row["nocheck"] = 0;    // 被保険者番号＝*******（エラー） →  nocheck＝０ （チェック済み）にする。
                        }
                        else if(Convert.ToInt32(row["noninput"]) == 0 && row["hiho"].ToString() != "-------")
                        {
                            using(var dt2 = GetTotsuData(y:bill[0], m:bill[1], taba:bill[2], hiho:row["hiho"].ToString(), ko:row["ko"].ToString(), cost:row["totalcost"].ToString(), medimonth:row["medimonth"].ToString()))
	                        {
		                        if(dt2 != null && dt2.Rows.Count == 1 && (row["rid"].ToString() != dt2.Rows[0]["処理番号"].ToString() || Convert.ToInt32(row["nocheck"]) != 0))
                                {
                                    row["rid"] = dt2.Rows[0]["処理番号"];
                                    row["nocheck"] = 0;
                                }
	                        }
                        }
	                }

                    UpdateZokushi(dt1);

                    using (var cheanged = dt1.GetChanges())
                    {
                        if (cheanged != null) da.Update(cheanged);
                    }
	            }
            }
            catch (Exception ex)
            {
                Logs.writeErr("エラーが発生しました。", true);
                Logs.writeErr(ex, false);
            }
        }

        static private void UpdateZokushi(DataTable dt)
        {
            DataRow genshi = null;      // 原紙データ

            foreach (DataRow row in dt.Rows)
            {
                if (row["hiho"].ToString() != "-------" && row["hiho"].ToString() != "*******")
                {
                    // 被保険者番号が ------- （続紙）、*******（エラー）以外 → 原紙とみなす。
                    genshi = row;
                }
                else if (genshi != null && row["hiho"].ToString() == "-------")
                {
                    // 被保険者番号が ------- （続紙）→ 原紙をコピーする。
                    row["rid"] = genshi["rid"];
                    row["nocheck"] = genshi["nocheck"];
                }
            }
        }

        /// <summary> 続紙への紐付けを行う。</summary>
        /// <pparam name="group">画像グループ名</pparam>
        static public void UpdateZokushi(string group)
        {
            try
            {
                using (var dt = new DataTable())
                using (var da = SQLClass.GetDbDataAdapter("SELECT * FROM receipts WHERE gname = :gname ORDER BY FNAME"))
                using (da.UpdateCommand = SQLClass.CreateCommand("UPDATE receipts SET rid = :rid, nocheck = :nocheck WHERE gname = :gname AND fname = :fname"))
                {
                    da.SelectCommand.Parameters.Add(":gname", NpgsqlTypes.NpgsqlDbType.Text).Value = group;
                    da.UpdateCommand.Parameters.Add(":rid", NpgsqlTypes.NpgsqlDbType.Text).SourceColumn = "rid";
                    da.UpdateCommand.Parameters.Add(":nocheck", NpgsqlTypes.NpgsqlDbType.Integer).SourceColumn = "nocheck";
                    da.UpdateCommand.Parameters.Add(":gname", NpgsqlTypes.NpgsqlDbType.Text).SourceColumn = "gname";
                    da.UpdateCommand.Parameters.Add(":fname", NpgsqlTypes.NpgsqlDbType.Text).SourceColumn = "fname";

                    
                    da.Fill(dt);
                    dt.AcceptChanges();

                    UpdateZokushi(dt);

                    using(var changed = dt.GetChanges()){
                        if (changed != null) da.Update(changed);
                    }

                }
            }
            catch (Exception ex)
            {
                Logs.writeErr("エラーが発生しました。", true);
                Logs.writeErr(ex, false);
            }
        }

        
        
        /// <summary> 入力の対象となる画像のグループ名 </summary>
        string m_Group;

        /// <summary> 入力の対象となる画像のファイル名リスト </summary>
        string[] m_FNameList;

        /// <summary> 入力の対象となる画像のファイル名インデックス </summary>
        int m_FNameIndex;

        /// <summary> 処理番号 ()</summary>
        string m_rid;

        public FormCheckFix(string group, string[] fnameList)
        {
            m_Group = group;
            m_FNameList = fnameList;
            m_FNameIndex = 0;
            InitializeComponent();

        }

        private void FormInput_Load(object sender, EventArgs e)
        {
            try
            {
                imagePreviewTool.Init(C_GlobalData.getAppDir() + @"\InputWindowSizeDataCheck.xml",
                                      new[] { userControlImage1, userControlImage2},
                                      new[] { splitContainer1, splitContainer2});
                ShowImage();
            }
            catch (Exception ex)
            {
                Logs.writeErr("エラーが発生しました。", true);
                Logs.writeErr(ex, false);
                Close();
            }

        }



        private void FormCheckFix_FormClosing(object sender, FormClosingEventArgs e)
        {
            imagePreviewTool.Exit();
        }

        void ShowImage()
        {
            if (m_FNameIndex >= m_FNameList.Length)
            {
                MessageBox.Show("入力完了！");
                m_FNameIndex = m_FNameList.Length - 1;
            }

            buttonPrev.Visible = (m_FNameIndex > 0);

            // レセプトの情報を表示
            using (var c = SQLClass.CreateCommand("SELECT * FROM receipts WHERE gname = :g AND fname = :f"))
            {
                c.Parameters.Add("g", NpgsqlTypes.NpgsqlDbType.Text);
                c.Parameters.Add("f", NpgsqlTypes.NpgsqlDbType.Text);
                c.Parameters["g"].Value = m_Group;
                c.Parameters["f"].Value = m_FNameList[m_FNameIndex];

                using (var r = c.ExecuteReader())
                {
                    r.Read();
                    m_rid = r["rid"].ToString();
                    textBoxHiho.Text = r["hiho"].ToString();
                    textBoxKo.Text = r["ko"].ToString();
                    //textBoxMedimonth.Text = r["medimonth"].ToString();
                    //textBoxCost.Text = r["totalcost"].ToString();
                }
            }

            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();


            imagePreviewTool.ShowImage(C_GlobalData.GetPictureDir() + @"\" + m_Group + @"\" + m_FNameList[m_FNameIndex]);
            textBoxHiho.Select();

            UpdateDataGiridView();
        }


        private void textBox_Enter(object sender, EventArgs e)
        {
            try
            {
                ((TextBox)sender).BackColor = Color.Yellow;
            }
            catch (Exception)
            {
            }
        }

        private void textBox_Leave(object sender, EventArgs e)
        {
            try
            {
                ((TextBox)sender).BackColor = Color.White;
            }
            catch (Exception)
            {
            }

        }

        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down || e.KeyCode == Keys.Enter)
            {
                this.ProcessTabKey(true);
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Up && ((TextBox)sender).Name != textBoxHiho.Name)
            {
                this.ProcessTabKey(false);
                e.Handled = true;
            }
        }



        /// <summary> 被保険者番号がエラー OR 続紙の場合、上書き保存する。 </summary>
        void SaveHihoZoku()
        {
            if (textBoxHiho.Text != "-------" && textBoxHiho.Text != "*******") return;
            string query = "UPDATE receipts SET noninput = 0, nocheck = :c, rid = :r, medimonth = :m, hiho = :h, totalcost = :t, ko=:k WHERE gname = :g AND fname = :f";

            using (var cmd = SQLClass.CreateCommand(query))
            {
                cmd.Parameters.Add(":c", NpgsqlTypes.NpgsqlDbType.Integer).Value = (textBoxHiho.Text == "-------" ? 1 : 0);     // 続紙→未チェック（１）、続紙以外→チェック済み（０）
                cmd.Parameters.Add(":r", NpgsqlTypes.NpgsqlDbType.Text).Value = null;                                        // 処理番号なし
                cmd.Parameters.Add(":m", NpgsqlTypes.NpgsqlDbType.Text).Value = null;                                        // 診査年月なし
                cmd.Parameters.Add(":t", NpgsqlTypes.NpgsqlDbType.Text).Value = null;                                        // 決定金額なし
                cmd.Parameters.Add(":k", NpgsqlTypes.NpgsqlDbType.Text).Value = null;                                        // 子番号なし
                cmd.Parameters.Add(":h", NpgsqlTypes.NpgsqlDbType.Text).Value = textBoxHiho.Text;                               // 被保険者番号 ＝ 入力データ
                cmd.Parameters.Add(":g", NpgsqlTypes.NpgsqlDbType.Text).Value = m_Group;
                cmd.Parameters.Add(":f", NpgsqlTypes.NpgsqlDbType.Text).Value = m_FNameList[m_FNameIndex];
                cmd.ExecuteNonQuery();
            }

        }

        /// <summary> 戻るボタン </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonPrev_Click(object sender, EventArgs e)
        {
            SaveHihoZoku();

            // 一つ前に戻る
            if (m_FNameIndex > 0)
            {
                m_FNameIndex--;
                ShowImage();
            }
        }

        /// <summary> 次へボタン </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonNext_Click(object sender, EventArgs e)
        {
            SaveHihoZoku();

            m_FNameIndex++;
            ShowImage();

        }


        private void button1_Click(object sender, EventArgs e)
        {
            SaveHihoZoku();
            using (var dlg = new FormCheckSel(m_Group, m_FNameList, m_FNameIndex))
            {
                dlg.ShowDialog();
                if (dlg.SelectedIndex >= 0 && dlg.SelectedIndex < m_FNameList.Length)
                {
                    m_FNameIndex = dlg.SelectedIndex;
                    ShowImage();
                }

            }
        }
        
        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {

                if (e.RowIndex >= 0)
                {
                    string rid = dataGridView1["処理番号", e.RowIndex].Value.ToString();

                    if (rid == m_rid)
                    {
                        e.CellStyle.BackColor = Color.Cyan;
                        e.CellStyle.SelectionBackColor = e.CellStyle.BackColor;
                        e.CellStyle.SelectionForeColor = e.CellStyle.ForeColor;
                    }


                }
            }
            catch (Exception)
            {
                // 正常系の例外
            }
        }

        /// <summary>突合データの取得</summary>
        /// <param name="y">審査年</param>
        /// <param name="m">診査月</param>
        /// <param name="taba">束番号</param>
        /// <param name="hiho">被保険者番号</param>
        /// <param name="ko">子番号（空白を無視）</param>
        /// <param name="cost">決定金額（空白を無視）</</param>
        /// <param name="medimonth">施療年月（空白を無視）</param>
        /// <returns>突合データ（処理番号、子番号表示、生年月日表示、氏名表示、施療年月、機関名、決定金額）</returns>
        private static DataTable GetTotsuData(int y, int m, int taba, string hiho, string ko = "", string cost = "", string medimonth = "")
        {
            int n;
            ko = (int.TryParse(ko.Trim(), out n) && n > 0) ? n.ToString("000") : "";                    // 子番号の整形  （整数３桁）
            cost = (int.TryParse(cost.Trim(), out n) && n > 0) ? n.ToString("") : "";                   // 決定金額の整形（整数）
            medimonth = (int.TryParse(medimonth.Trim(), out n) && n > 0) ? n.ToString("0000") : "";     // 施療年月の整形（整数４桁）

            DataTable dt = new DataTable();

            /// 診査年月、決定金額、子番号で突合させる。
            string query = " SELECT 処理番号,  被保険者番号, " +
                           " 束番号 || '-' || 子番号 AS 番号, 束番号, 子番号, " +
                           " 生年月日年号 || 生年月日年 || '年' || 生年月日月 || '月' || 生年月日日 || '日' AS 生年月日, " +
                           " 受診者氏名, 施療年月, 機関名, 決定金額 FROM tdata " +
                           " WHERE 審査年 = :y AND 審査月 = :m AND 被保険者番号 = :h ";
            if(ko.Length > 0) query += " AND 子番号 = :ko ";     // 子番号入力ありの場合
            query += " ORDER BY 束番号, 子番号";

            using (var da = SQLClass.GetDbDataAdapter(query))
            {
                da.SelectCommand.Parameters.Add(":y", NpgsqlTypes.NpgsqlDbType.Text).Value = y.ToString();
                da.SelectCommand.Parameters.Add(":m", NpgsqlTypes.NpgsqlDbType.Text).Value = m.ToString();
                da.SelectCommand.Parameters.Add(":h", NpgsqlTypes.NpgsqlDbType.Text).Value = hiho;
                da.SelectCommand.Parameters.Add(":ko", NpgsqlTypes.NpgsqlDbType.Text).Value = ko;
                da.Fill(dt);
            }

            // 束番号でフィルタリングする。
            if (dt.Rows.Count > 1) tdataSelect(dt, "束番号='" + taba.ToString() + "'");

            // 決定金額・施療年月でフィルタリングする。
            if (dt.Rows.Count > 1)
            {
                if (cost.Length > 0 && medimonth.Length > 0) tdataSelect(dt, "決定金額='" + cost + "' AND 施療年月 = '" + medimonth + "'");
                else if (cost.Length > 0) tdataSelect(dt, "決定金額='" + cost + "'");
                else if (medimonth.Length > 0) tdataSelect(dt, "施療年月 = '" + medimonth + "'");
            }
            return dt;
        }

        /// <summary>DataTableを条件で絞り込む。</summary>
        /// 該当データありの場合に限る。条件から外れたデータを削除する。
        /// <param name="dt"></param>
        /// <param name="filterExpression">条件式</param>
        static private void tdataSelect(DataTable dt, string filterExpression)
        {
            var rows = dt.Select(filterExpression);
            if (rows.Length > 0 && rows.Length < dt.Rows.Count)
            {
                var keys = new HashSet<string>();
                foreach (DataRow row in rows) keys.Add(row["処理番号"].ToString());

                for (int i = dt.Rows.Count - 1; i >= 0; i--)
                {
                    if (keys.Contains(dt.Rows[i]["処理番号"].ToString()) == false) dt.Rows.RemoveAt(i);
                }
            }
        }

        private void buttonSeek_Click(object sender, EventArgs e)
        {
            UpdateDataGiridView();
        }

        private void UpdateDataGiridView()
        {
            int[] bill = GetBillMonth(m_Group);
            using (var dt = GetTotsuData(y: bill[0], m: bill[1], taba:bill[2], 
                                         hiho: textBoxHiho.Text.Trim(),
                                         ko: textBoxKo.Text))
            //using (var dt = GetTotsuData(y: bill[0], m: bill[1],
            //                             hiho: textBoxHiho.Text.Trim(),
            //                             ko: textBoxKo.Text,
            //                             cost: textBoxCost.Text,
            //                             medimonth: textBoxMedimonth.Text))
            {
                // DataGridView初期化(ボタン列を挿入)
                dataGridView1.Columns.Clear();
                dataGridView1.Columns.Add(new DataGridViewButtonColumn());
                dataGridView1.Columns[0].Name = "番号";
                dataGridView1.Columns[0].DataPropertyName = "番号";


                dataGridView1.DataSource = dt;
                dataGridView1.Columns["受診者氏名"].HeaderText = "受診者";
                dataGridView1.Columns["施療年月"].HeaderText = "施療月";
                dataGridView1.Columns["決定金額"].HeaderText = "金額";
                dataGridView1.Columns["処理番号"].Visible = false;
                dataGridView1.Columns["被保険者番号"].Visible = false;
                dataGridView1.Columns["束番号"].Visible = false;
                dataGridView1.Columns["子番号"].Visible = false;

                dataGridView1.Sort(dataGridView1.Columns["子番号"], ListSortDirection.Ascending);


                foreach (DataGridViewColumn col in dataGridView1.Columns)
                {
                    col.HeaderCell.Style.WrapMode = DataGridViewTriState.False;
                }

                // フィントサイズの縮小
                dataGridView1.ColumnHeadersDefaultCellStyle.Font = label5.Font;     // ヘッダ行
                dataGridView1.Columns["機関名"].DefaultCellStyle.Font = label5.Font;
                //foreach (DataGridViewColumn item in dataGridView1.Columns)
                //{
                //    if(item.Visible && item.Name != "子番号") item.DefaultCellStyle.Font = label5.Font;
                //}

                dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);
                //buttoncolumn.Width += 40;
            }
        }

        /// <summary> 選択ボタンクリック時 </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            
            //"子番号"列ならば、ボタンがクリックされた
            if (dgv.Columns[e.ColumnIndex].Name == "番号" && e.RowIndex >= 0)
            {
                int ko1, ko2;
//                int ko1, ko2, medimonth1, medimonth2, cost1, cost2;

                // 入力値（子番号、施療年月、決定金額）を取得する。
                if(int.TryParse(textBoxKo.Text.Trim(), out ko1) == false) ko1 = -1;
                //if(int.TryParse(textBoxMedimonth.Text.Trim(), out medimonth1) == false) medimonth1 = -1;
                //if (int.TryParse(textBoxCost.Text.Trim(), out cost1) == false) cost1 = -1;

                // 突合データの値入力値（子番号、施療年月、決定金額）を取得する。
                if (int.TryParse(dgv["子番号", e.RowIndex].Value.ToString(), out ko2) == false) ko2 = -1;
                //if (int.TryParse(dgv["施療年月", e.RowIndex].Value.ToString(), out medimonth2) == false) medimonth2 = -1;
                //if (int.TryParse(dgv["決定金額", e.RowIndex].Value.ToString(), out cost2) == false) cost2 = -1;


                using (var cmd = SQLClass.CreateCommand("UPDATE receipts SET rid=:rid, hiho=:hiho, ko=:ko, noninput=0, nocheck=0 WHERE gname=:gname AND fname=:fname"))
                //using (var cmd = SQLClass.CreateCommand("UPDATE receipts SET rid=:rid, hiho=:hiho, ko=:ko, medimonth=:medimonth, totalcost=:totalcost, noninput=0, nocheck=0 WHERE gname=:gname AND fname=:fname"))
                {
                    cmd.Parameters.Add(":rid", NpgsqlTypes.NpgsqlDbType.Text).Value = dgv["処理番号", e.RowIndex].Value;
                    cmd.Parameters.Add(":hiho", NpgsqlTypes.NpgsqlDbType.Text).Value = dgv["被保険者番号", e.RowIndex].Value;

                    cmd.Parameters.Add(":ko", NpgsqlTypes.NpgsqlDbType.Text).Value = (ko1 == ko2 ? ko1.ToString("000") : "");
                    //cmd.Parameters.Add(":medimonth", NpgsqlTypes.NpgsqlDbType.Text).Value = (medimonth1 == medimonth2 ? medimonth1.ToString("0000") : "");
                    //cmd.Parameters.Add(":totalcost", NpgsqlTypes.NpgsqlDbType.Text).Value = (cost1 == cost2 ? cost1.ToString("") : ""); 
                        
                    cmd.Parameters.Add(":gname", NpgsqlTypes.NpgsqlDbType.Text).Value = m_Group;
                    cmd.Parameters.Add(":fname", NpgsqlTypes.NpgsqlDbType.Text).Value = m_FNameList[m_FNameIndex];
                    SQLClass.DebugCommandText(cmd);
                    cmd.ExecuteNonQuery();

                }


                m_rid = dgv["処理番号", e.RowIndex].Value.ToString();

                dataGridView1.Refresh();

                //Save();
            }
        }

        private void buttonRotate_Click(object sender, EventArgs e)
        {
            imagePreviewTool.RotateImage(((Control)sender).Name == buttonClockwise.Name);
        }

        private void textBoxHiho_Validating(object sender, CancelEventArgs e)
        {
            if (textBoxHiho.Text == "-------" || textBoxHiho.Text == "*******")
            {
                // 被保険者番号がエラー OR 続紙の場合、上書き保存する。 
                string query = "UPDATE receipts SET noninput = 0, nocheck = :c, rid = :r, medimonth = :m, hiho = :h, totalcost = :t, ko=:k WHERE gname = :g AND fname = :f";
                using (var cmd = SQLClass.CreateCommand(query))
                {
                    cmd.Parameters.Add(":c", NpgsqlTypes.NpgsqlDbType.Integer).Value = (textBoxHiho.Text == "-------" ? 1 : 0);     // 続紙→未チェック（１）、続紙以外→チェック済み（０）
                    cmd.Parameters.Add(":r", NpgsqlTypes.NpgsqlDbType.Text).Value = null;                                           // 処理番号なし
                    cmd.Parameters.Add(":m", NpgsqlTypes.NpgsqlDbType.Text).Value = null;                                           // 診査年月なし
                    cmd.Parameters.Add(":t", NpgsqlTypes.NpgsqlDbType.Text).Value = null;                                           // 決定金額なし
                    cmd.Parameters.Add(":k", NpgsqlTypes.NpgsqlDbType.Text).Value = null;                                           // 子番号なし
                    cmd.Parameters.Add(":h", NpgsqlTypes.NpgsqlDbType.Text).Value = textBoxHiho.Text;                               // 被保険者番号 ＝ 入力データ
                    cmd.Parameters.Add(":g", NpgsqlTypes.NpgsqlDbType.Text).Value = m_Group;
                    cmd.Parameters.Add(":f", NpgsqlTypes.NpgsqlDbType.Text).Value = m_FNameList[m_FNameIndex];
                    cmd.ExecuteNonQuery();
                }

                m_rid = "";
                dataGridView1.DataSource = null;

                e.Cancel = false;
            }
            else if (textBoxHiho.Text.Length <= 0)
            {
                /// 空白の場合は無視する。
                e.Cancel = false;
            }
            else
            {
                using (var cmd = SQLClass.CreateCommand("SELECT COUNT(*) FROM tdata WHERE 被保険者番号 = :hiho"))
                {
                    cmd.Parameters.Add(":hiho", NpgsqlTypes.NpgsqlDbType.Text).Value = textBoxHiho.Text.Trim();
                    using (var r = cmd.ExecuteReader())
                    {
                        r.Read();
                        if (Convert.ToInt32(r[0]) <= 0)
                        {
                            e.Cancel = true;
                            textBoxHiho.SelectAll();
                        }
                        else
                        {
                            e.Cancel = false;
                        }
                    }
                }
            }
        }


    }
}
