﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows;
using System.Threading;

namespace AmaInput
{
    public partial class FormTablePreview : Form
    {
        public FormTablePreview()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 起動時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormTablePreview_Load(object sender, EventArgs e)
        {
            C_GlobalData.DataGridView_AddCripbordCopyMenu(dataGridView1);
            C_GlobalData.SetComboChangedFunc(comboBoxBillMonth, 500, OnBillMonthChanged);
            C_GlobalData.SetComboChangedFunc(comboBoxTaba, 500, OnTabaChanged);

            try
            {
                using(var dt = new DataTable())
                using (var da = SQLClass.GetDbDataAdapter(" SELECT 審査年, 審査月, count(*) FROM tdata " +
                                                          " GROUP BY 審査年, 審査月 ORDER BY 審査年 DESC, 審査月 DESC " ))
                {
                    da.Fill(dt);
                    var comboData = new string[dt.Rows.Count];
                    comboData[0] = "審査月";
                    for (int i = 0; i < dt.Rows.Count; i++) comboData[i] = dt.Rows[i][0] + "年" + dt.Rows[i][1] + "月審査" + "(" + dt.Rows[i][2] + "件)";
                    comboBoxBillMonth.DataSource = comboData;
                }                
            }
            catch (Exception ex)
            {
                Logs.writeErr("エラーが発生しました", true);
                Logs.writeErr(ex, false); 
                Close();
            }
        }

        /// <summary>
        /// 審査月選択変更時の処理
        /// </summary>
        /// <param name="combo"></param>
        /// <returns></returns>
        bool OnBillMonthChanged(ComboBox combo)
        {
            try
            {
                using (var dt = new DataTable())
                using (var da = SQLClass.GetDbDataAdapter(" SELECT 束番号, count(*) AS 件数 FROM tdata " +
                                                          " WHERE 審査年 = :y AND 審査月 = :m GROUP BY 束番号 ORDER BY 束番号 "))
                {
                    string[] month = combo.Text.Split('年','月');
                    da.SelectCommand.Parameters.Add("y", NpgsqlTypes.NpgsqlDbType.Text).Value = month[0];
                    da.SelectCommand.Parameters.Add("m", NpgsqlTypes.NpgsqlDbType.Text).Value = month[1];
                    da.Fill(dt);

                    var comboData = new string[dt.Rows.Count + 1];
                    comboData[0] = "束番号";
                    for (int i = 0; i < dt.Rows.Count; i++) comboData[i + 1] = dt.Rows[i][0] +  "束目 (" + dt.Rows[i][1] + "件)";
                    comboBoxTaba.DataSource = comboData;
                    comboBoxTaba.SelectedIndex = 1;
                }

                return true;
            }
            catch (Exception ex)
            {
                Logs.writeErr(ex, false);
                return false;
            }

        }



        /// <summary>
        /// 束番号選択変更時の処理
        /// </summary>
        /// <param name="combo"></param>
        /// <returns></returns>
        bool OnTabaChanged(ComboBox combo)
        {
            try
            {
                string query = " SELECT * FROM tdata " +
                               " WHERE 審査年 = :y AND 審査月 = :m ";
                if (comboBoxTaba.SelectedIndex > 0) query += " AND 束番号 = :t ";
                query += "ORDER BY 束番号, 子番号 ";


                using (var dt = new DataTable())
                using (var da = SQLClass.GetDbDataAdapter(query))
                {
                    string[] month = comboBoxBillMonth.Text.Split('年', '月');
                    da.SelectCommand.Parameters.Add("y", NpgsqlTypes.NpgsqlDbType.Text).Value = month[0];
                    da.SelectCommand.Parameters.Add("m", NpgsqlTypes.NpgsqlDbType.Text).Value = month[1];
                    if (comboBoxTaba.SelectedIndex > 0) da.SelectCommand.Parameters.Add("t", NpgsqlTypes.NpgsqlDbType.Text).Value = comboBoxTaba.Text.Split('束')[0];
                    da.Fill(dt);

                    dataGridView1.DataSource = dt;
                    int i = 0;
                    dataGridView1.Columns["審査月"].DisplayIndex = i++;
                    dataGridView1.Columns["審査年"].DisplayIndex = i++;
                    dataGridView1.Columns["束番号"].DisplayIndex = i++;
                    dataGridView1.Columns["子番号"].DisplayIndex = i++;
                    dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);
                }

                return true;
            }
            catch (Exception ex)
            {
                Logs.writeErr(ex, false);
                return false;
            }
        }









    }
}
