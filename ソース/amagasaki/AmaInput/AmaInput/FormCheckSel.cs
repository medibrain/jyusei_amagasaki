﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AmaInput
{
    public partial class FormCheckSel : Form
    {
        public int SelectedIndex = -1;

        private int m_DefIndex;

        public FormCheckSel(string gname, string[] m_FnameList, int defIndex)
        {
            m_DefIndex = defIndex;

            InitializeComponent();


            string query = "SELECT HIHO, ko, "
                         + " 審査年 || '年' || 審査月 || '月 ' || 束番号 || '-' || 子番号 AS 束番号, " 
                         + " 受診者氏名, 施療年月, 決定金額, 機関名 FROM receipts " 
                         + " LEFT JOIN tdata ON 処理番号 = rid AND 被保険者番号 = hiho "
                         + " WHERE gname = :gname AND fname = :fname";

            using (var dt = new DataTable())
            using(var da = SQLClass.GetDbDataAdapter(query))
            {
                da.SelectCommand.Parameters.Add(":gname", NpgsqlTypes.NpgsqlDbType.Text) .Value = gname;
                da.SelectCommand.Parameters.Add(":fname", NpgsqlTypes.NpgsqlDbType.Text);

                foreach (var item in m_FnameList)
	            {
                    da.SelectCommand.Parameters[":fname"].Value = item;
                    da.Fill(dt);
	            }

                dt.Columns.Add("NO");
                for (int n = 0; n < dt.Rows.Count; n++) dt.Rows[n]["NO"] = n + 1;

                dataGridView1.Columns.Clear();
                dataGridView1.Columns.Add(new DataGridViewButtonColumn());
                dataGridView1.Columns[0].Name = "NO";
                dataGridView1.Columns[0].DataPropertyName = "NO";

                dataGridView1.DataSource = dt;


                foreach (DataGridViewColumn col in dataGridView1.Columns) col.SortMode = DataGridViewColumnSortMode.NotSortable;                

                dataGridView1.Columns["HIHO"].HeaderText = "被保番";
                dataGridView1.Columns["HIHO"].DefaultCellStyle.BackColor = Color.LightBlue;
                dataGridView1.Columns["ko"].HeaderText = "子番号";
                dataGridView1.Columns["ko"].DefaultCellStyle.BackColor = Color.LightBlue;
                dataGridView1.Columns["受診者氏名"].HeaderText = "氏名";

            }

        }



        private void FormCheckSel_Load(object sender, EventArgs e)
        {


            dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            
            //"NO"列ならば、ボタンがクリックされた
            if (dgv.Columns[e.ColumnIndex].Name == "NO" && e.RowIndex >= 0)
            {
                SelectedIndex = e.RowIndex;
                Close();
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                SelectedIndex = e.RowIndex;
                Close();
            }
        }

        private void FormCheckSel_Shown(object sender, EventArgs e)
        {
            dataGridView1.CurrentCell = dataGridView1["NO", m_DefIndex];
        }


    }
}
