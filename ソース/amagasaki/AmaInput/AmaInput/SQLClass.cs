﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Data.Common;
using System.IO;
using Npgsql;


/// <summary> SQLコネクションクラス （コンパイルスイッチにて、PostgreSQL と SQLightを切り替える）</summary>
public class SQLClass
{
    static NpgsqlConnection con = null;

    /// <summary> 接続OPEN </summary>
    public static void Open()
    {
        con = new NpgsqlConnection(C_GlobalData.ConnectText());
        con.Open();
    }

    public static void Close()
    {
        if (con != null)
        {
            con.Close();
            con.Dispose();
            con = null;
        }
    }

    /// <summary> SQLコマンドを作成する。 </summary>
    /// <param name="commandText"> コマンドテキスト </param>
    /// <returns> SQLコマンド </returns>
    public static NpgsqlCommand CreateCommand(string commandText)
    {
        return new NpgsqlCommand(commandText, con);
    }

    /// <summary> DbDataAdapterの取得 </summary>
    /// <param name="commandText"> コマンドテキスト </param>
    /// <returns> </returns>
    public static NpgsqlDataAdapter GetDbDataAdapter(string commandText)
    {
        return new NpgsqlDataAdapter(commandText, con);
    }

    /// <summary>
    /// トランザクションを開始します
    /// </summary>
    /// <returns></returns>
    public static DbTransaction BeginTran()
    {
        return con.BeginTransaction();
    }

    public static void DebugCommandText(Npgsql.NpgsqlCommand cmd)
    {
        try
        {
            string query = cmd.CommandText;
            foreach (Npgsql.NpgsqlParameter p in cmd.Parameters)
            {
                if (p.NpgsqlDbType == NpgsqlTypes.NpgsqlDbType.Text) query = query.Replace(p.ParameterName, "'" + p.Value.ToString() + "'");
                else query = query.Replace(p.ParameterName, p.Value.ToString());
            }

            System.Diagnostics.Trace.WriteLine(query);
        }
        catch (Exception ex)
        {
            System.Diagnostics.Trace.WriteLine(ex.Message + "\r\n" + ex.StackTrace);
            throw;
        }


    }

}
