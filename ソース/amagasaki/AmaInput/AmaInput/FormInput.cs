﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AmaInput
{
    public partial class FormInput : Form
    {
        ImagePreviewTool imagePreviewTool = new ImagePreviewTool();

        int m_cnt;

        /// <summary>審査年</summary>
        int billY = 0;
        
        /// <summary>審査月</summary>
        int billM = 0;

        /// <summary>画像グループ名（束番号）</summary>
        string m_Group
        {
            get { return labelBosatsu.Text; }
            set { 
                labelBosatsu.Text = value;
                string[] s = value.Split('-');
                billY = int.Parse(s[0]);            // 診療年
                billM = int.Parse(s[1]) + 1;        // 診療月
                if (billM > 12)
                {
                    billY++;
                    billM -= 12;
                }
            }
        }

        string _FName_;

        string m_FName
        {
            get { return _FName_; }
            set
            {
                _FName_ = value;
                labelFname.Text = System.IO.Path.GetFileNameWithoutExtension(value).Split('-').Last() + " / " +
                                  m_cnt.ToString("000");
            }
        }
        
        public FormInput(string group, int cnt)
        {
            InitializeComponent();

            m_cnt = cnt;
            m_Group = group;
        }


        private void FormInput_Load(object sender, EventArgs e)
        {
            try
            {
                imagePreviewTool.Init(C_GlobalData.getAppDir() + @"\InputWindowSize.xml",
                                      new[] { userControlImage1, userControlImage2, userControlImage3, userControlImage4},
                                      new[] { splitContainer1, splitContainer2, splitContainer3 });

                bool res;

                using (var cmd = SQLClass.CreateCommand("SELECT * FROM receipts WHERE gname = :gname ORDER BY noninput DESC, fname LIMIT 1"))
                {
                    cmd.Parameters.Add("gname", NpgsqlTypes.NpgsqlDbType.Text);
                    cmd.Parameters["gname"].Value = m_Group;


                    using (var r = cmd.ExecuteReader())
                    {
                        res = r.Read();
                        if (res) ShowImage(r);
                        else
                        {
                            MessageBox.Show("エラーが発生しました。");
                            Close();
                        }
                    }
                }

                if (res) ShowBarcodeData();

            }
            catch (Exception)
            {
            }
        }



        private void FormInput_FormClosing(object sender, FormClosingEventArgs e)
        {
            imagePreviewTool.Exit();
        }


        void ShowImage(System.Data.Common.DbDataReader r)
        {
            m_FName = r["fname"].ToString();
            textBoxHiho.Text = r["hiho"].ToString();
            textBoxKo.Text = r["ko"].ToString();
            labelName.Text = "";                            // バーコード氏名
            
            //textBox1.Text = r["medimonth"].ToString();            
            //textBox3.Text = r["totalcost"].ToString();
            imagePreviewTool.ShowImage(C_GlobalData.GetPictureDir() + @"\" + m_Group + @"\" + m_FName);
            
            textBoxHiho.Select();
        }

        /// <summary>
        /// 被保険者番号が未入力の場合、バーコード解析データを表示する。
        /// </summary>
        private void ShowBarcodeData()
        {
            string barHiho, barName;
            int barKo;

            // バーコード解析
            if (textBoxHiho.Text.Length <= 0 && readBar(userControlImage1.Image, billY, billM, out barHiho, out barName, out barKo) == true)
            {
                // 被保険者番号が未入力、バーコードあり → 自動入力する
                labelName.Text = barName;                               // 氏名
                textBoxHiho.Text = barHiho;                             // 被保険者番号
                if (barKo > 0) textBoxKo.Text = barKo.ToString("000");   // 子番号
            }
        }
        

        private void textBox_Enter(object sender, EventArgs e)
        {
            try
            {
                ((TextBox)sender).BackColor = Color.Yellow;
            }
            catch (Exception)
            {
            }
        }

        private void textBox_Leave(object sender, EventArgs e)
        {
            try
            {
                ((TextBox)sender).BackColor = Color.White;
            }
            catch (Exception)
            {
            }

        }

        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down || e.KeyCode == Keys.Enter)
            {
                this.ProcessTabKey(true);
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Up && ((TextBox)sender).Name != textBoxHiho.Name)
            {
                this.ProcessTabKey(false);
                e.Handled = true;
            }
        }

        static Npgsql.NpgsqlCommand cmd = null;

        /// <summary> 入力チェックと保存の実行 </summary>
        void Save()
        {
            if(textBoxHiho.Text.Length <= 0) return;      // 被保険者番号が空白 → 保存しない

            // 子番号のチェック （正数以外は空白とみなす）
            int ko;
            if(int.TryParse(textBoxKo.Text.Trim(), out ko) == false || ko < 0) ko =0;


            if (cmd == null)
            {
                // SQLの更新コマンド（被保険者番号と子番号を更新する）
                cmd = SQLClass.CreateCommand("UPDATE receipts SET noninput = 0, hiho = :h, ko = :k WHERE gname = :g AND fname = :f");

                cmd.Parameters.Add("h", NpgsqlTypes.NpgsqlDbType.Text);         // h:被保険者番号
                cmd.Parameters.Add("k", NpgsqlTypes.NpgsqlDbType.Text);         // k:被保険者番号
                cmd.Parameters.Add("g", NpgsqlTypes.NpgsqlDbType.Text);         // g:画像グループ名
                cmd.Parameters.Add("f", NpgsqlTypes.NpgsqlDbType.Text);         // f:画像ファイル名
            }

            cmd.Parameters["g"].Value = m_Group;
            cmd.Parameters["f"].Value = m_FName;

            cmd.Parameters["h"].Value = textBoxHiho.Text;
            if (textBoxHiho.Text == "-------" || textBoxHiho.Text == "*******" || ko <= 0) cmd.Parameters["k"].Value = "";  // 続紙 or エラー → 子番号を空白にする。
            else cmd.Parameters["k"].Value = ko.ToString("000");
            
            cmd.ExecuteNonQuery();
        }

        /// <summary> 戻るボタン </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonPrev_Click(object sender, EventArgs e)
        {
            // 保存の実行（ただし、被保険者番号入力ありに限る）
            Save();

            bool res;

            // 一つ前に戻る
            using (var c = SQLClass.CreateCommand("SELECT * FROM receipts WHERE gname = :g AND fname < :f ORDER BY fname DESC LIMIT 1"))
            {
                c.Parameters.Add("g", NpgsqlTypes.NpgsqlDbType.Text);
                c.Parameters.Add("f", NpgsqlTypes.NpgsqlDbType.Text);
                c.Parameters["g"].Value = m_Group;
                c.Parameters["f"].Value = m_FName;

                using (var r = c.ExecuteReader())
                {
                    res = r.Read();
                    if (res)　ShowImage(r);              // 該当あり → 一つ前を表示
                    else buttonPrev.Visible = false;     // 該当なし → 戻るボタンを隠す
                }
            }

            if (res) ShowBarcodeData();
        }

        /// <summary> OKボタン （次へ進む）</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (textBoxHiho.Text.Trim().Length <= 0)
            {
                textBoxHiho.Select();
                textBoxHiho.SelectAll();
                return;
            }

            Save();

            
            buttonPrev.Visible = true;              // 次へ進むので、戻るボタンは表示しておく


            bool res;

            // 次データを表示する
            using (var c = SQLClass.CreateCommand("SELECT * FROM receipts WHERE gname = :g AND fname > :f ORDER BY fname LIMIT 1"))
            {
                c.Parameters.Add("g", NpgsqlTypes.NpgsqlDbType.Text);
                c.Parameters.Add("f", NpgsqlTypes.NpgsqlDbType.Text);
                c.Parameters["g"].Value = m_Group;
                c.Parameters["f"].Value = m_FName;

                using (var r = c.ExecuteReader())
                {
                    res = r.Read();
                    if (res) ShowImage(r);         // 該当あり → 次データを表示
                }
                
                if (res)
                {
                    ShowBarcodeData();
                    return;
                }
            }

            // 次データなしの場合は、未入力を探して表示する
            using (var c = SQLClass.CreateCommand("SELECT * FROM receipts WHERE gname = :g AND noninput = 1 ORDER BY fname LIMIT 1"))
            {
                c.Parameters.Add("g", NpgsqlTypes.NpgsqlDbType.Text);
                c.Parameters["g"].Value = m_Group;

                using (var r = c.ExecuteReader())
                {
                    res = r.Read();
                    if (res)　ShowImage(r);         // 該当あり → 次データを表示
                    else MessageBox.Show("入力完了");
                }

                if (res) ShowBarcodeData();
            }
        }

        private void buttonRotate_Click(object sender, EventArgs e)
        {
            imagePreviewTool.RotateImage(((Control)sender).Name == buttonClockwise.Name);
        }

        /// <summary>被保険者番号の入力チェック</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxHiho_Validating(object sender, CancelEventArgs e)
        {
            if (textBoxHiho.Text.Length <= 0 || textBoxHiho.Text == "-------" || textBoxHiho.Text == "*******")
            {
                /// 空白の場合は無視する。
                e.Cancel = false;
            }
            else
            {
                using (var cmd = SQLClass.CreateCommand("SELECT COUNT(*) FROM tdata WHERE 被保険者番号 = :hiho"))
                {
                    cmd.Parameters.Add(":hiho", NpgsqlTypes.NpgsqlDbType.Text).Value = textBoxHiho.Text.Trim();
                    using (var r = cmd.ExecuteReader())
                    {
                        r.Read();
                        if (Convert.ToInt32(r[0]) <= 0)
                        {
                            e.Cancel = true;
                            textBoxHiho.SelectAll();
                        }
                        else
                        {
                            e.Cancel = false;
                        }
                    }
                }
            }
        }

        /// <summary>被保番がエラーor続紙の場合、子番号入力を省略</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxHiho_TextChanged(object sender, EventArgs e)
        {
            textBoxKo.TabStop = (textBoxHiho.Text != "*******" && textBoxHiho.Text != "-------");
        }




        
        /// <summary>
        /// バーコードの読み込み
        /// </summary>
        /// <param name="img">画像</param>
        /// <param name="y">審査年</param>
        /// <param name="m">審査月</param>
        /// <param name="hiho">[OUT]被保険者番号</param>
        /// <param name="name">[OUT]氏名</param>
        /// <param name="ko">[OUT]子番号</param>
        /// <returns>TRUE成功、FALSE失敗</returns>
        static bool readBar(Image img, int y, int m, out string hiho, out string name, out int ko)
        {
            var barlist = readBarText(img);
            if (barlist == null)
            {
                hiho = name = "";
                ko = 0;
                return false;            // バーコードなし
            }

            // バーコードを解析する。
            foreach (var item in barlist)
            {
                bool res = analyzeBarText(item, y, m, out hiho, out name, out ko);
                if (res)
                {
                    // 成功 → バーコードの内容をログに残し、今後の解析の為の参考にする。
                    Logs.writeQrLog(item + "(" + hiho + "-" + ko.ToString("000") + "-" + name + ")");
                    return res;
                }
            }

            // 解析失敗 → バーコードの内容をログに残し、今後の解析の為の参考にする。
            foreach (var item in barlist) Logs.writeQrErr(item);


            hiho = name = "";
            ko = 0;
            return false;            // バーコードなし
        }


        static ZXing.BarcodeReader barcodeReader = null;

        /// <summary>
        /// 画像→バーコード一覧を取得する。
        /// </summary>
        /// <param name="img">画像全体</param>
        /// <returns>バーコードの内容（テキスト）一覧 or NULL</returns>
        static string[] readBarText(Image img)
        {
            try
            {
                if (barcodeReader == null)
                {
                    /// バーコードリーダーの設定 （最適化のため、CODABARのみにする。）
                    barcodeReader = new ZXing.BarcodeReader { AutoRotate = false, TryInverted = false };
                    barcodeReader.Options.PossibleFormats = new ZXing.BarcodeFormat[] { ZXing.BarcodeFormat.QR_CODE };
                }

                // 画像は、下部２０％の領域を抽出する。
                using (var bmp1 = new Bitmap(img))
//                using (var bmp2 = bmp1.Clone(new Rectangle(0, (int)(bmp1.Height * 0.8), (int)(bmp1.Width * 0.25), (int)(bmp1.Height * 0.2)), bmp1.PixelFormat))
                using (var bmp2 = bmp1.Clone(new Rectangle(0, (int)(bmp1.Height * 0.8), bmp1.Width, (int)(bmp1.Height * 0.2)), bmp1.PixelFormat))
                {
                    var bars = barcodeReader.DecodeMultiple(bmp2);
                    if (bars == null) return null;
                    else if (bars.Length <= 0) return null;

                    var text = new string[bars.Length];
                    for (int i = 0; i < bars.Length; i++) text[i]=bars[i].Text;
                    return text;
                }
            }
            catch (Exception ex)
            {
                Logs.writeErr(ex, false);
                return null;
                throw;
            }
        }

        /// <summary>
        /// バーコードのテキストを解析
        /// </summary>
        /// <param name="str">バーコードテキスト</param>
        /// <param name="y">審査年</param>
        /// <param name="m">審査月</param>
        /// <param name="hiho">[OUT]被保険者番号</param>
        /// <param name="name">[OUT]氏名</param>
        /// <param name="ko">[OUT]子番号</param>
        /// <returns>TRUE成功、FALSE失敗</returns>
        static bool analyzeBarText(string str, int y, int m, out string hiho, out string name, out int ko)
        {
            try
            {
                hiho = "";
                name = "";
                ko = 0;

                str = str.Replace('　', ' ').Trim();                        // 全角スペース → 半角スペースに変換する
                while (str.Contains("  ")) str = str.Replace("  ", " ");    // スペース連続 → スペース単独に置換
                string[] text = str.Split(' ');                             // 半角スペースで区切る
                        
                // [0]不明 [1]不明 [2～最後-2]氏, 名, カナ氏, カナ名 [最後-1]被保険者番号 [最後]不明 の形式かどうかを判断
                if (text.Length < 5) return false;                          // 被保険者番号なし
                if (text[text.Length - 2].Length != 7) return false;         // 被保険者番号が７桁以外

                // 被保険者番号と審査年月で、tdataと突合させる。
                using (var dt = new DataTable())
                using (var da = SQLClass.GetDbDataAdapter("SELECT * FROM tdata WHERE 被保険者番号 = :hiho AND 審査年 = :y AND 審査月 = :m"))
                {
                    da.SelectCommand.Parameters.Add("y", NpgsqlTypes.NpgsqlDbType.Text).Value = y.ToString();
                    da.SelectCommand.Parameters.Add("m", NpgsqlTypes.NpgsqlDbType.Text).Value = m.ToString();
                    da.SelectCommand.Parameters.Add("hiho", NpgsqlTypes.NpgsqlDbType.Text).Value = text[text.Length - 2];
                    da.Fill(dt);

                    if (dt.Rows.Count <= 0) return false;       // 該当の被保険者番号なし（正しくない。）

                    hiho = text[text.Length - 2];               // 被保番の突合OK！

                    DataRow row = null;
                    if (dt.Rows.Count == 1)
                    {
                        row = dt.Rows[0];           
                    }
                    else 
                    {
                        try 
	                    {	        
	                        // 苗字と名前で突合させる。
                            var rows = dt.Select("受診者氏名 LIKE '" + text[2] + "%' AND 受診者氏名 LIKE '%" + text[3] + "%'");
                            if(rows.Length==1) row=rows[0];
	                    }
	                    catch (Exception)
	                    {
                            // この例外は無視する。
	                    }
                    }

                    if(row != null)
                    {
                        name = row["受診者氏名"].ToString();
                        ko = int.Parse(row["子番号"].ToString());
                    }
                    else
                    {
                        name = text[2];
                        for (int i = 3; i <= text.Length - 3; i++) name += " " + text[i];
                        ko = 0;
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logs.writeErr(ex, false);
                hiho = "";
                name = "";
                ko = 0;
                return false;
            }

        }


        ///// <summary>
        ///// テストデータ作成　、　バーコードなしの画像 → DBから削除、 バーコードありの画像⇒未入力状態にする。
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void button1_Click(object sender, EventArgs e)
        //{
        //    using (var dt = new DataTable())
        //    using (var da = SQLClass.GetDbDataAdapter("SELECT * FROM receipts WHERE gname = :gname"))
        //    using (var del = SQLClass.CreateCommand("DELETE FROM receipts WHERE gname = :gname AND fname = :fname"))
        //    using (var upd = SQLClass.CreateCommand("UPDATE receipts SET noninput = 1, nocheck=1, nooutput=1, rid = null, " + 
        //                                            "medimonth=null, hiho=null, totalcost=null, ko=null WHERE gname = :gname AND fname = :fname"))
        //    {
        //        da.SelectCommand.Parameters.Add(":gname", NpgsqlTypes.NpgsqlDbType.Text).Value = m_Group;
        //        da.Fill(dt);

        //        del.Parameters.Add(":gname", NpgsqlTypes.NpgsqlDbType.Text).Value = m_Group;
        //        del.Parameters.Add(":fname", NpgsqlTypes.NpgsqlDbType.Text);

        //        upd.Parameters.Add(":gname", NpgsqlTypes.NpgsqlDbType.Text).Value = m_Group;
        //        upd.Parameters.Add(":fname", NpgsqlTypes.NpgsqlDbType.Text);

        //        foreach (DataRow row in dt.Rows)
        //        {
        //            string path = C_GlobalData.GetPictureDir() + @"\" + m_Group + @"\" + row["fname"].ToString();


        //            using(var img = Image.FromFile(path))
        //            {
        //                var bars = readBarList(img);
        //                if (bars != null)
        //                {
        //                    upd.Parameters[":fname"].Value = row["fname"].ToString();
        //                    upd.ExecuteNonQuery();
        //                }
        //                else
        //                {
        //                    del.Parameters[":fname"].Value = row["fname"].ToString();
        //                    del.ExecuteNonQuery();
        //                }
        //            }
        //        }
        //    }
        //    MessageBox.Show("テストデータ作成\r\nようやく終わった!");
        //}




    }
}
