﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AmaInput
{
    public partial class FormGroupSel : Form
    {
        public string SelectedGroup = "";
        public int SelectedCnt;

        /// <summary> チェックボックスONの時の絞り込み条件  例： HAVING sum(nocheck) = 0 </summary>
        string m_having1 = "";

        /// <summary> チェックボックスOFFの時の絞り込み条件  例： HAVING sum(nocheck) = 0 </summary>
        string m_having2 = "";

        /// <summary>ダブルクリックを有効にするかどうか</summary>
        bool m_EnableDoubleClick;

        /// <summary></summary>
        /// <param name="title">フォームのテキスト</param>
        /// <param name="checkboxText">チェックボックスのテキスト</param>
        /// <param name="having1">チェックボックスONの時の絞り込み条件  例： HAVING sum(nocheck) = 0  </param>
        /// <param name="having2">チェックボックスOFFの時の絞り込み条件  例： HAVING sum(nocheck) = 0 </param>
        /// <param name="okText">OKボタンのテキスト</param>
        /// <param name="enableDoubleClick">ダブルクリックを有効にするかどうか</param>
        public FormGroupSel(string title, string checkboxText, string having1, string having2, string okText="OK", bool enableDoubleClick = true)
        {
            InitializeComponent();
            this.Text = title;
            m_having1 = having1;
            m_having2 = having2;
            checkBox1.Text = checkboxText;
            checkBox1.Checked = true;
            button1.Text = okText;
            m_EnableDoubleClick = enableDoubleClick;
        }

        private void FormGroupSel_Load(object sender, EventArgs e)
        {
            UpdateView();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            UpdateView();
        }

        void UpdateView()
        {
            string query = "SELECT gname AS グループ, count(*) as 枚数, sum(noninput) as 未入力, sum(nocheck) as 未チェック, sum(nooutput) as 未出力 FROM receipts group by gname";
            if (checkBox1.Checked) query += " " + m_having1;
            else query += " " + m_having2;

            query += " ORDER BY left(gname, 5) desc, gname ";

            using(var da = SQLClass.GetDbDataAdapter(query))
            using (var dt = new DataTable())
            {
                da.Fill(dt);
                dataGridView1.DataSource = dt;
                dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedGroup = dataGridView1.SelectedRows[0].Cells["グループ"].Value.ToString();
                SelectedCnt = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells["枚数"].Value);
                Close();
            }
            catch (Exception)
            {
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try 
	        {
                if (m_EnableDoubleClick)
                {
                    SelectedGroup = dataGridView1["グループ", e.RowIndex].Value.ToString();
                    SelectedCnt = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells["枚数"].Value);
                    Close();
                }
	        }
	        catch (Exception)
	        {
		
		        
	        }
        }

    }
}
