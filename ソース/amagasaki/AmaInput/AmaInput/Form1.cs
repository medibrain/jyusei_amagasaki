﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AmaInput
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            labelVer.Text = "尼崎市柔整システム\r\n" +
                            ",ﾊﾞｰｼﾞｮﾝ：" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version + 
                            System.IO.File.GetLastWriteTime(System.Reflection.Assembly.GetExecutingAssembly().Location).ToString("(yyyy-MM-dd HH:mm:ss)") + "\r\n" +
                            string.Join("\r\n", C_GlobalData.GetIniData());
        }

        /// <summary> 国保連データ取込 </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                // ファイル一覧を選択する。
                openFileDialog1.Title = "国保連データ取込";
                openFileDialog1.Multiselect = true;
                openFileDialog1.Filter = "|*.csv";
                if (openFileDialog1.ShowDialog() != DialogResult.OK) return;


                this.Visible = false;
                
                using (var dlg = new ReadCsvForm(openFileDialog1.FileNames))
                {
                    dlg.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                Logs.writeErr("エラーが発生しました。", true);
                Logs.writeErr(ex, false);
            }
            
            this.Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (var dlg = new FormTablePreview()) dlg.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Visible = false;
            using (var dlg = new ScanRename()) dlg.ShowDialog();
            Visible = true;
        }

        /// <summary> 入力 </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            string group;
            int cnt;
            using (var dlg = new FormGroupSel("入力グループ選択", "未入力のみ", "HAVING sum(noninput) > 0", ""))
            {
                dlg.ShowDialog();

                group = dlg.SelectedGroup;
                cnt = dlg.SelectedCnt;
            }

            if (group != "")
            {
                using (var dlg = new FormInput(group, cnt))
                {
                    dlg.ShowDialog();
                }
            }
        }

        /// <summary>一括削除ボタン</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDeleteGroup_Click(object sender, EventArgs e)
        {
            using (var dlg = new FormGroupSel("削除グループ選択", "未入力のみ", "HAVING sum(noninput) > 0", "", "削除実行", false))
            {
                dlg.ShowDialog();
                if (dlg.SelectedGroup != "")
                {
                    try
                    {
                        System.IO.Directory.Delete(C_GlobalData.GetPictureDir() + @"\" + dlg.SelectedGroup, true);
                        using (var c = SQLClass.CreateCommand("DELETE FROM receipts WHERE gname = :g"))
                        {
                            c.Parameters.AddWithValue("g", dlg.SelectedGroup);
                            c.ExecuteNonQuery();
                        }

                        MessageBox.Show("削除しました。\r\nグループ：" + dlg.SelectedGroup + "\r\n" + dlg.SelectedCnt + "枚", "削除完了");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "削除失敗", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }


                }
            }
        }

        /// <summary> 入力チェック・修正</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button5_Click(object sender, EventArgs e)
        {
            string group;
            using (var dlg = new FormGroupSel("入力チェック", "未チェックのみ", "HAVING sum(noninput) = 0 AND sum(nocheck) > 0", "HAVING sum(noninput) = 0"))
            {
                dlg.ShowDialog();
                group = dlg.SelectedGroup;

                if (group.Length > 0) FormCheckFix.DataCheck(group);
            }
            
            // 入力済・未チェックのデータを探す
            var list = new List<string>();          // 未チェックのファイルリスト
            string query = "SELECT * FROM receipts WHERE gname = :g AND noninput = 0 AND nocheck = 1 ORDER BY fname";     // 再チェック以外
            using (var c = SQLClass.CreateCommand(query))
            {
                c.Parameters.Add("g", NpgsqlTypes.NpgsqlDbType.Text).Value = group;
                using (var r = c.ExecuteReader())
                {
                    while (r.Read())
                    {
                        if (r["medimonth"].ToString() != "****" && r["medimonth"].ToString() != "----") list.Add(r["fname"].ToString());
                    }
                }
            }

            if (list.Count > 0)using (var dlg = new FormCheckFix(group, list.ToArray())) dlg.ShowDialog();
            FormCheckFix.UpdateZokushi(group);
            if (list.Count <= 0)  MessageBox.Show("チェック完了");
        }


        /// <summary> チェック取消 </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button6_Click(object sender, EventArgs e)
        {
            string group;
            using (var dlg = new FormGroupSel("チェック取消", "チェック済のみ", "HAVING sum(nocheck) = 0 AND sum(noninput) = 0", "HAVING sum(noninput) = 0"))
            {
                dlg.ShowDialog();
                group = dlg.SelectedGroup;
            }
            if (group == "") return;


            using (var c = SQLClass.CreateCommand("UPDATE receipts SET rid = null, nocheck = 1 WHERE gname = :g"))
            {
                c.Parameters.Add("g", NpgsqlTypes.NpgsqlDbType.Text);
                c.Parameters["g"].Value = group;
                c.ExecuteNonQuery();
            }
        }


        /// <summary> チェック済み画像の再確認 </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button7_Click(object sender, EventArgs e)
        {

            string group;
            using (var dlg = new FormGroupSel("画像確認", "チェック済のみ", "HAVING sum(nocheck) = 0", ""))
            {
                dlg.ShowDialog();
                group = dlg.SelectedGroup;
            }
            if (group == "") return;


            // 該当のデータを探す
            var list = new List<string>();          // 未チェックのファイルリスト
            using (var c = SQLClass.CreateCommand("SELECT * FROM receipts WHERE gname = :g ORDER BY fname"))
            {
                c.Parameters.Add("g", NpgsqlTypes.NpgsqlDbType.Text);
                c.Parameters["g"].Value = group;
                using (var r = c.ExecuteReader())
                {
                    while (r.Read())
                    {
                        list.Add(r["fname"].ToString());
                    }
                }
            }
            if (list.Count > 0) using (var dlg = new FormCheckFix(group, list.ToArray())) dlg.ShowDialog();
            FormCheckFix.UpdateZokushi(group);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            using (var dlg = new FormTenken()) dlg.ShowDialog();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            using (var dlg = new FormOutput()) dlg.ShowDialog();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            using (var dlg = new FormNoScanCheck()) dlg.ShowDialog();
        }


    }
}
