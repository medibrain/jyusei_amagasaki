﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AmaInput
{
    public partial class FormTenken : Form
    {
        public FormTenken()
        {
            InitializeComponent();

            var h = new EventHandler(textBoxChanged);
            comboBoxHos.SelectionChangeCommitted += h;              // 施術師コード変更時
            comboBoxHos.Validated += h;
            comboBoxName.SelectionChangeCommitted += h;             // 氏名変更時
            comboBoxName.Validated += h;
            userControlTenken1.textBoxBillMonth.TextChanged += h;   // 審査月１変更時
            userControlTenken2.textBoxBillMonth.TextChanged += h;   // 審査月２変更時

            labelPreview1.Text = labelPreview2.Text = "";
        }

        private void FormTenken_Load(object sender, EventArgs e)
        {
        }

        string prevHiho = "";
        string prevHos = "";
        string prevName = "";
        string prevMonth1 = "";
        string prevMonth2 = "";

        /// <summary> 被保番・審査月などの変更時の処理→画像一覧を更新する！ </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxChanged(object sender, EventArgs e)
        {
            try
            {
                // 被保番チェック （空白・６桁未満の時は更新しない！）
                if (textBoxHiho.Text.Trim().Length != textBoxHiho.MaxLength) return;

                // 被保険者番号変更時 →
                if (((Control)sender).Name == textBoxHiho.Name && prevHiho != textBoxHiho.Text)
                {
                    // 被保険者番号変更時 → 受診者氏名と健診機関名の一覧を更新する！
                    string name = comboBoxName.Text;
                    string hos = comboBoxHos.Text;

                    comboBoxName.Items.Clear();
                    comboBoxHos.Items.Clear();

                    using (var c = SQLClass.CreateCommand("SELECT DISTINCT 受診者氏名 FROM tdata WHERE 被保険者番号 = :code"))
                    {
                        c.Parameters.Add("code", NpgsqlTypes.NpgsqlDbType.Text);
                        c.Parameters["code"].Value = textBoxHiho.Text;
                        using (var r = c.ExecuteReader())
                        {
                            while (r.Read()) comboBoxName.Items.Add(r[0]);
                        }
                    }

                    using (var c = SQLClass.CreateCommand("SELECT DISTINCT 機関名 FROM tdata WHERE 被保険者番号 = :code"))
                    {
                        c.Parameters.Add("code", NpgsqlTypes.NpgsqlDbType.Text);
                        c.Parameters["code"].Value = textBoxHiho.Text;
                        using (var r = c.ExecuteReader())
                        {
                            while (r.Read()) comboBoxHos.Items.Add(r[0]);
                        }
                    }

                    comboBoxName.Text = name;
                    comboBoxHos.Text = hos;

                }// if(((Control)sender).Name == textBoxHiho.Name) 被保険者番号更新時の処理（氏名・健診機関一覧の更新）はココまで


                // 被保番変更チェック
                bool chkHiho = (prevHiho != textBoxHiho.Text || prevHos != comboBoxHos.Text || prevName != comboBoxName.Text);  

                // 審査月変更チェック
                bool chk1 = (userControlTenken1.textBoxBillMonth.Text.Trim().Length == 4) && (chkHiho || prevMonth1 != userControlTenken1.textBoxBillMonth.Text);
                bool chk2 = (userControlTenken2.textBoxBillMonth.Text.Trim().Length == 4) && (chkHiho || prevMonth2 != userControlTenken2.textBoxBillMonth.Text);
                

                // 検索キーの設定表示する画像一覧を変更する！
                string query = " SELECT * FROM tdata, receipts "
                             + " WHERE rid = 処理番号 AND 被保険者番号 = :被保険者番号 "
                             + " AND 審査年 = :審査年 AND 審査月 = :審査月 ";
                if (comboBoxName.Text != "") query += " AND 受診者氏名 = :受診者氏名 ";
                if (comboBoxHos.Text != "") query += " AND 機関番号 IN (SELECT DISTINCT 機関番号 FROM tdata WHERE 機関名 = :機関名)";

                query += " ORDER BY gname, fname";

                using (var c = SQLClass.CreateCommand(query))
                {
                    c.Parameters.Add("審査年", NpgsqlTypes.NpgsqlDbType.Text);
                    c.Parameters.Add("審査月", NpgsqlTypes.NpgsqlDbType.Text);
                    c.Parameters.Add("被保険者番号", NpgsqlTypes.NpgsqlDbType.Text).Value = textBoxHiho.Text.Trim();
                    c.Parameters.Add("受診者氏名", NpgsqlTypes.NpgsqlDbType.Text).Value = comboBoxName.Text;
                    c.Parameters.Add("機関名", NpgsqlTypes.NpgsqlDbType.Text).Value = comboBoxHos.Text;


                    if (chk1)
                    {
                        ShowPreview(userControlTenken1, c);
                        prevMonth1 = userControlTenken1.textBoxBillMonth.Text;
                    }

                    if (chk2)
                    {
                        ShowPreview(userControlTenken2, c);
                        prevMonth2 = userControlTenken2.textBoxBillMonth.Text;
                    }
                }

                if (chkHiho)
                {
                    prevHiho = textBoxHiho.Text;
                    prevHos = comboBoxHos.Text;
                    prevName = comboBoxName.Text;
                }
            }
            catch (Exception ex)
            {
                Logs.writeErr(ex, false);
            }
        }
        
        void ShowPreview(UserControlTenken form, Npgsql.NpgsqlCommand c)
        {
            c.Parameters["審査年"].Value = form.textBoxBillMonth.Text.Substring(0, 2);
            c.Parameters["審査月"].Value = form.textBoxBillMonth.Text.Substring(2, 2).TrimStart('0');
            var imgList = new List<UserControlTenken.PreviewData>();

            using (var r = c.ExecuteReader())
            {
                while (r.Read())
                {
                    imgList.Add(new UserControlTenken.PreviewData()
                    {
                        img = C_GlobalData.GetPictureDir() + @"\" + r["gname"] + @"\" + r["fname"],



                        text = r["審査年"] + "年" + r["審査月"] + "月-" + r["束番号"] + "-" + r["子番号"] + "\r\n"
                             + r["受診者氏名"] + "\r\n" + r["機関名"]
                    }
                    );
                }
            }
            form.SetPreviewList(imgList);
        }
    }
}
