﻿namespace AmaInput
{
    partial class FormTenken
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.comboBoxHos = new System.Windows.Forms.ComboBox();
            this.comboBoxName = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxHiho = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.labelPreview1 = new System.Windows.Forms.Label();
            this.labelPreview2 = new System.Windows.Forms.Label();
            this.userControlTenken1 = new UserControlTenken();
            this.userControlTenken2 = new UserControlTenken();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelPreview2);
            this.panel1.Controls.Add(this.labelPreview1);
            this.panel1.Controls.Add(this.comboBoxHos);
            this.panel1.Controls.Add(this.comboBoxName);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.textBoxHiho);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(512, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(130, 332);
            this.panel1.TabIndex = 0;
            // 
            // comboBoxHos
            // 
            this.comboBoxHos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHos.FormattingEnabled = true;
            this.comboBoxHos.Location = new System.Drawing.Point(3, 171);
            this.comboBoxHos.Name = "comboBoxHos";
            this.comboBoxHos.Size = new System.Drawing.Size(121, 20);
            this.comboBoxHos.TabIndex = 2;
            // 
            // comboBoxName
            // 
            this.comboBoxName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxName.FormattingEnabled = true;
            this.comboBoxName.Location = new System.Drawing.Point(3, 113);
            this.comboBoxName.Name = "comboBoxName";
            this.comboBoxName.Size = new System.Drawing.Size(121, 20);
            this.comboBoxName.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 156);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "施術所";
            // 
            // textBoxHiho
            // 
            this.textBoxHiho.Font = new System.Drawing.Font("ＭＳ ゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxHiho.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxHiho.Location = new System.Drawing.Point(5, 35);
            this.textBoxHiho.MaxLength = 7;
            this.textBoxHiho.Name = "textBoxHiho";
            this.textBoxHiho.Size = new System.Drawing.Size(80, 26);
            this.textBoxHiho.TabIndex = 1;
            this.textBoxHiho.TextChanged += new System.EventHandler(this.textBoxChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "氏名";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "被保番(必須)";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.userControlTenken1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.userControlTenken2);
            this.splitContainer1.Size = new System.Drawing.Size(512, 332);
            this.splitContainer1.SplitterDistance = 253;
            this.splitContainer1.TabIndex = 1;
            // 
            // labelPreview1
            // 
            this.labelPreview1.AutoSize = true;
            this.labelPreview1.Location = new System.Drawing.Point(3, 204);
            this.labelPreview1.Name = "labelPreview1";
            this.labelPreview1.Size = new System.Drawing.Size(83, 12);
            this.labelPreview1.TabIndex = 3;
            this.labelPreview1.Text = "labelPreview1";
            // 
            // labelPreview2
            // 
            this.labelPreview2.AutoSize = true;
            this.labelPreview2.Location = new System.Drawing.Point(3, 274);
            this.labelPreview2.Name = "labelPreview2";
            this.labelPreview2.Size = new System.Drawing.Size(83, 12);
            this.labelPreview2.TabIndex = 3;
            this.labelPreview2.Text = "labelPreview2";
            // 
            // userControlTenken1
            // 
            this.userControlTenken1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.userControlTenken1.PreviewTextControl = this.labelPreview1;
            this.userControlTenken1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlTenken1.Location = new System.Drawing.Point(0, 0);
            this.userControlTenken1.Name = "userControlTenken1";
            this.userControlTenken1.Size = new System.Drawing.Size(253, 332);
            this.userControlTenken1.TabIndex = 0;
            // 
            // userControlTenken2
            // 
            this.userControlTenken2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.userControlTenken2.PreviewTextControl = this.labelPreview2;
            this.userControlTenken2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlTenken2.Location = new System.Drawing.Point(0, 0);
            this.userControlTenken2.Name = "userControlTenken2";
            this.userControlTenken2.Size = new System.Drawing.Size(255, 332);
            this.userControlTenken2.TabIndex = 0;
            // 
            // FormTenken
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(642, 332);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Name = "FormTenken";
            this.Text = "レセプト点検";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormTenken_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox comboBoxHos;
        private System.Windows.Forms.ComboBox comboBoxName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxHiho;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private UserControlTenken userControlTenken1;
        private UserControlTenken userControlTenken2;
        private System.Windows.Forms.Label labelPreview2;
        private System.Windows.Forms.Label labelPreview1;
    }
}