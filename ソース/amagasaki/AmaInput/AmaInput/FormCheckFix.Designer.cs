﻿namespace AmaInput
{
    partial class FormCheckFix
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxKo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonCounterclockwise = new System.Windows.Forms.Button();
            this.buttonClockwise = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxHiho = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonSeek = new System.Windows.Forms.Button();
            this.buttonPrev = new System.Windows.Forms.Button();
            this.buttonNext = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.userControlImage1 = new UserControlImage();
            this.userControlImage2 = new UserControlImage();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxKo
            // 
            this.textBoxKo.Font = new System.Drawing.Font("ＭＳ ゴシック", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxKo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBoxKo.Location = new System.Drawing.Point(273, 68);
            this.textBoxKo.MaxLength = 3;
            this.textBoxKo.Name = "textBoxKo";
            this.textBoxKo.Size = new System.Drawing.Size(41, 28);
            this.textBoxKo.TabIndex = 10;
            this.textBoxKo.Enter += new System.EventHandler(this.textBox_Enter);
            this.textBoxKo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
            this.textBoxKo.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(232, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "子番号";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(134, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 24);
            this.label5.TabIndex = 8;
            this.label5.Text = "続紙[-------]\r\nエラー[*******]";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(187, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(62, 31);
            this.button1.TabIndex = 3;
            this.button1.Text = "一覧";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonCounterclockwise
            // 
            this.buttonCounterclockwise.BackColor = System.Drawing.Color.White;
            this.buttonCounterclockwise.Font = new System.Drawing.Font("MS UI Gothic", 9F);
            this.buttonCounterclockwise.Image = global::AmaInput.Properties.Resources.ImageCounterclockwise;
            this.buttonCounterclockwise.Location = new System.Drawing.Point(64, 5);
            this.buttonCounterclockwise.Name = "buttonCounterclockwise";
            this.buttonCounterclockwise.Size = new System.Drawing.Size(50, 48);
            this.buttonCounterclockwise.TabIndex = 1;
            this.buttonCounterclockwise.UseVisualStyleBackColor = false;
            this.buttonCounterclockwise.Click += new System.EventHandler(this.buttonRotate_Click);
            // 
            // buttonClockwise
            // 
            this.buttonClockwise.BackColor = System.Drawing.Color.White;
            this.buttonClockwise.Font = new System.Drawing.Font("MS UI Gothic", 9F);
            this.buttonClockwise.Image = global::AmaInput.Properties.Resources.ImageClockwise;
            this.buttonClockwise.Location = new System.Drawing.Point(119, 5);
            this.buttonClockwise.Name = "buttonClockwise";
            this.buttonClockwise.Size = new System.Drawing.Size(50, 48);
            this.buttonClockwise.TabIndex = 2;
            this.buttonClockwise.UseVisualStyleBackColor = false;
            this.buttonClockwise.Click += new System.EventHandler(this.buttonRotate_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(5, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 0;
            this.label6.Text = "画像回転";
            // 
            // textBoxHiho
            // 
            this.textBoxHiho.Font = new System.Drawing.Font("ＭＳ ゴシック", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxHiho.Location = new System.Drawing.Point(45, 68);
            this.textBoxHiho.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxHiho.MaxLength = 7;
            this.textBoxHiho.Name = "textBoxHiho";
            this.textBoxHiho.Size = new System.Drawing.Size(88, 28);
            this.textBoxHiho.TabIndex = 7;
            this.textBoxHiho.Enter += new System.EventHandler(this.textBox_Enter);
            this.textBoxHiho.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
            this.textBoxHiho.Leave += new System.EventHandler(this.textBox_Leave);
            this.textBoxHiho.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxHiho_Validating);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(4, 70);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 24);
            this.label2.TabIndex = 6;
            this.label2.Text = "被保番\r\n7桁";
            // 
            // buttonSeek
            // 
            this.buttonSeek.Location = new System.Drawing.Point(324, 59);
            this.buttonSeek.Margin = new System.Windows.Forms.Padding(4);
            this.buttonSeek.Name = "buttonSeek";
            this.buttonSeek.Size = new System.Drawing.Size(63, 39);
            this.buttonSeek.TabIndex = 11;
            this.buttonSeek.TabStop = false;
            this.buttonSeek.Text = "検索";
            this.buttonSeek.UseVisualStyleBackColor = true;
            this.buttonSeek.Click += new System.EventHandler(this.buttonSeek_Click);
            // 
            // buttonPrev
            // 
            this.buttonPrev.Location = new System.Drawing.Point(267, 12);
            this.buttonPrev.Margin = new System.Windows.Forms.Padding(4);
            this.buttonPrev.Name = "buttonPrev";
            this.buttonPrev.Size = new System.Drawing.Size(61, 32);
            this.buttonPrev.TabIndex = 4;
            this.buttonPrev.TabStop = false;
            this.buttonPrev.Text = "戻る";
            this.buttonPrev.UseVisualStyleBackColor = true;
            this.buttonPrev.Click += new System.EventHandler(this.buttonPrev_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.Location = new System.Drawing.Point(328, 12);
            this.buttonNext.Margin = new System.Windows.Forms.Padding(4);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(61, 32);
            this.buttonNext.TabIndex = 5;
            this.buttonNext.Text = "次へ";
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 30;
            this.dataGridView1.Size = new System.Drawing.Size(391, 186);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.buttonNext);
            this.panel3.Controls.Add(this.textBoxKo);
            this.panel3.Controls.Add(this.buttonPrev);
            this.panel3.Controls.Add(this.button1);
            this.panel3.Controls.Add(this.textBoxHiho);
            this.panel3.Controls.Add(this.buttonCounterclockwise);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.buttonClockwise);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.buttonSeek);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(393, 105);
            this.panel3.TabIndex = 1;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.userControlImage1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Panel2.Controls.Add(this.panel3);
            this.splitContainer1.Size = new System.Drawing.Size(795, 454);
            this.splitContainer1.SplitterDistance = 398;
            this.splitContainer1.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 105);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.dataGridView1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.userControlImage2);
            this.splitContainer2.Size = new System.Drawing.Size(393, 349);
            this.splitContainer2.SplitterDistance = 188;
            this.splitContainer2.TabIndex = 0;
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Image = null;
            this.userControlImage1.ImageLocation = null;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(396, 452);
            this.userControlImage1.TabIndex = 1;
            // 
            // userControlImage2
            // 
            this.userControlImage2.AutoScroll = true;
            this.userControlImage2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage2.Image = null;
            this.userControlImage2.ImageLocation = null;
            this.userControlImage2.Location = new System.Drawing.Point(0, 0);
            this.userControlImage2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.userControlImage2.Name = "userControlImage2";
            this.userControlImage2.Size = new System.Drawing.Size(391, 155);
            this.userControlImage2.TabIndex = 1;
            // 
            // FormCheckFix
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(795, 454);
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormCheckFix";
            this.Text = "入力";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormCheckFix_FormClosing);
            this.Load += new System.EventHandler(this.FormInput_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Button buttonPrev;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxHiho;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonSeek;
        private System.Windows.Forms.Button buttonCounterclockwise;
        private System.Windows.Forms.Button buttonClockwise;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxKo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private UserControlImage userControlImage1;
        private UserControlImage userControlImage2;
    }
}