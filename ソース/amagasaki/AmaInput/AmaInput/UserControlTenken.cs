﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

public partial class UserControlTenken : UserControl
{
    /// <summary>デバッグ表示用のテキストコントロール</summary>
    public Control PreviewTextControl { get; set; }

    /// <summary> 表示画像（フルパス名）リスト</summary>
    List<string> m_ImageList = new List<string>();

    public UserControlTenken()
    {
        InitializeComponent();
    }


    /// <summary> プレビュー対象のデータ </summary>
    public class PreviewData
    {
        /// <summary> 画像（フルパス名）</summary>
        public string img;

        /// <summary> プレビューテキスト（審査月・電算番号などを表示） </summary>
        public string text;
    }
    
    /// <summary> 表示画像リスト</summary>
    public List<PreviewData> m_List = null;

    public void SetPreviewList(List<PreviewData> list)
    {
        m_List = list;                          // 表示画像リストの設定
        m_Index = 0;                            // 表示画像インデックスの設定
        PreviewNow();                           // プレビュー表示
    }



    /// <summary>表示画像インデックス</summary>
    int m_Index = 0;

    /// <summary> 現在のデータの表示 </summary>
    void PreviewNow()
    {
        try
        {
            // Indexの訂正 （０～MAX（m_List.Count-1）までとする！）
            if (m_List == null) m_Index = -1;                               // 画像なし → Index = -1
            else if (m_List.Count <= 0) m_Index = -1;                       //    〃
            else if (m_Index < 0) m_Index = 0;                              // インデックス＜０  → ０に訂正
            else if (m_Index >= m_List.Count) m_Index = m_List.Count - 1;   // インデックス＞MAX → MAXに訂正


            // ボタン表示・非表示の切り替え
            buttonPrev.Enabled = (m_Index > 0);                                 // ＜ボタン：indexが１以上 （前データ有り）で表示
            buttonNext.Enabled = (m_Index >= 0 && m_Index < m_List.Count - 1);  // ＜ボタン：indexがMAX未満（次データ有り）で表示

            // カウントの表示
            if (m_Index < 0) labelCount.Text = "";
            else labelCount.Text = (m_Index + 1).ToString() + " / " + m_List.Count.ToString();


            // 画像表示
            userControlImage1.Image = null;
            if (m_Index >= 0) userControlImage1.ImageLocation = m_List[m_Index].img;
            else userControlImage1.Image = null;


            userControlImage1.InitPictureBoxLocation();

            // テキスト表示
            if (PreviewTextControl != null) PreviewTextControl.Text = ((m_Index >= 0) ? (m_List[m_Index].text) : (""));
        }
        catch (Exception)
        {
        }
    }
    private void buttonPrev_Click(object sender, EventArgs e)
    {
        m_Index--;
        PreviewNow();
    }

    private void buttonNext_Click(object sender, EventArgs e)
    {
        m_Index++;
        PreviewNow();
    }

    /// <summary>印刷ボタン処理</summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void button1_Click(object sender, EventArgs e)
    {
        try
        {
            if (userControlImage1.Image == null) return;

            printDialog1.Document = printDocument1;
            if (printDialog1.ShowDialog() == DialogResult.OK)
            {
                // 縦横指定
                printDocument1.DefaultPageSettings.Landscape = (userControlImage1.Image.Width > userControlImage1.Image.Height);
                printDocument1.Print();
            }
        }
        catch (Exception)
        {
        }
    }

    static Font printfont = new Font("MS Gothic", 80);

    private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
    {
        try
        {
            if (userControlImage1.Image == null)
            {
                e.Cancel = true;
                e.HasMorePages = false;
                return;
            }

            // 画像を取得
            using (var bmp = new Bitmap(userControlImage1.Image))
            {
                // 縦・横の調整 、画像が横向きのとき、90度回転させる！
                //if (bmp.Height < bmp.Width) bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);

                // 縮尺 （領域サイズ／画像サイズ）
                double w = e.PageBounds.Width - e.PageSettings.HardMarginX * 2;     // 印刷可能幅
                double h = e.PageBounds.Height - e.PageSettings.HardMarginY * 2;    // 印刷可能高さ
                double rate = Math.Min(w / bmp.Width, h / bmp.Height);              // 比率 （印刷可能サイズ÷画像サイズ）
                w = (float)(bmp.Width * rate);                                      // 印刷幅 ＝ 画像幅 ✕ 比率
                h = (float)(bmp.Height * rate);                                     // 印刷高 ＝ 画像高 ✕ 比率

                if (checkBoxStamp.Checked)
                {
                    // 先にスタンプを印字
                    e.Graphics.DrawString("参考申請書",
                          printfont,
                          Brushes.LightGray,
                          50,
                          50);
                }

                // 印刷位置のセンタリング (余白を調整する)
                // 余白 = 中心位置の差分 = (ページサイズ - 印刷サイズ ) / 2.0
                double x = (e.PageBounds.Width - w) / 2.0;
                double y = (e.PageBounds.Height - h) / 2.0f;

                // プレビューではなく実際の印刷の時はプリンタの物理的な余白分ずらす
                x -= e.PageSettings.HardMarginX;
                y -= e.PageSettings.HardMarginY;

                bmp.MakeTransparent(Color.White);
                // 印刷対象の画像のリストの現在ページを出力
                e.Graphics.DrawImage(bmp, (float)x, (float)y, (float)w, (float)h);
            }

            e.Cancel = false;
            e.HasMorePages = false;
        }
        catch (Exception)
        {
            e.Cancel = true;
            e.HasMorePages = false;
        }
    }
}

